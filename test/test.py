import numpy as np

num_vtx = 5
_membership_matrix = np.zeros((3, num_vtx), dtype='i4')
_membership_matrix[0][0] = 1
_membership_matrix[0][1] = 0
_membership_matrix[0][2] = 1
_membership_matrix[0][3] = 0
_membership_matrix[0][4] = 0

_membership_matrix[1][0] = 1
_membership_matrix[1][1] = 0
_membership_matrix[1][2] = 1
_membership_matrix[1][3] = 0
_membership_matrix[1][4] = 0

_membership_matrix[2][0] = 0
_membership_matrix[2][1] = 1
_membership_matrix[2][2] = 0
_membership_matrix[2][3] = 1
_membership_matrix[2][4] = 1

max_agreement_distribution = np.zeros( num_vtx )
for i in range( num_vtx ):
    max_label_agreements = np.bincount( _membership_matrix[:, i] ).argmax()
    max_agreement_distribution[i] = int(max_label_agreements)

print( max_agreement_distribution )