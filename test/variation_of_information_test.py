from math import log
import igraph as ig

def variation_of_information(X, Y):
  n = float(sum([len(x) for x in X]))
  sigma = 0.0
  for x in X:
    p = len(x) / n
    for y in Y:
      q = len(y) / n
      r = len(set(x) & set(y)) / n
      if r > 0.0:
        sigma += r * (log(r / p, 2) + log(r / q, 2))
  return abs(sigma)


# Identical partitions
X1 = [ [1,2,3,4,5], [6,7,8,9,10] ]
Y1 = [ [1,2,3,4,5], [6,7,8,9,10] ]
print(variation_of_information(X1, Y1))
# VI = 0

# Similar partitions
X2 = [ [1,2,3,4], [5,6,7,8,9,10] ]
Y2 = [ [1,2,3,4,5,6], [7,8,9,10] ]
print(variation_of_information(X2, Y2))
# VI = 1.102

# Dissimilar partitions
X3 = [ [1,2], [3,4,5], [6,7,8], [9,10] ]
Y3 = [ [10,2,3], [4,5,6,7], [8,9,1] ]
print(variation_of_information(X3, Y3))
# VI = 2.302

# Totally different partitions
X4 = [ [1,2,3,4,5,6,7,8,9,10] ]
Y4 = [ [1], [2], [3], [4], [5], [6], [7], [8], [9], [10] ]
print(variation_of_information(X4, Y4))
# VI = 3.322 (maximum VI is log(N) = log(10) = 3.322)

X5 = [ 1,1, 1, 1,2,2,3,3,3,5,5, 5, 5  ]
X6 = [ 1,1, 1, 1, 2,2,3,3,3,5,5, 5, 7 ]

print( ig.clustering.compare_communities(comm1=X5, comm2=X6, method='meila') )

X7 = [ 1,1,2,3,4,4,4 ]
X8 = [ 1,1,0,2,4,4,4 ]

print( ig.clustering.compare_communities(comm1=X7, comm2=X8, method='meila') )