from urllib.parse import urlsplit
import re

def format_url(url:str=None):
    if isinstance(url, str):
        url = url.lower()
        url = url.replace("http", "https")
        url = url.replace("httpss", "https")
        url = url.replace("https://www.", "https://")
        url = re.sub('\:[0-9]+', '', url) # handle port number
        url = urlunsplit(urlsplit(url)._replace(query="", fragment=""))
        url = url if url[-1]=="/" else url+"/" # add "/" as a final character if not present
    return url


urls = [
    #"http://difendersiora.it",
    #"http://difendersiora.it/",
    #"http://www.missdreamer.altervista.org/vaccino-",
    #"http://missdreamer.altervista.org/vaccino-pro-nobis",
    #"https://missdreamer.altervista.org/vaccino-pro-nobis/",
    'https://lanazione.it/arezzo/cronaca/va-in-arresto-cardiaco-dopo-il-vaccino-rianimato-in-terra-dal-medico-di-turno-1.6810186/amp?__twitter_impression=true&s=09/',
    'https://ilparagone.it/attualita/aifa-decessi-collegati-vaccino/?telegram/',
    'https://stackoverflow.com/questions/7990301?aaa=aaa',
    "http://www.missdreamer.altervista.org:4443/vaccino-pro-nobis",
    "http://www.missdreamer.altervista.org:443/vaccino-pro-nobis",
    "http://www.missdreamer.altervista.org:4/vaccino-pro-nobis",
    "http://www.missdreamer.altervista.org:/vaccino-pro-nobis",
]

from urllib.parse import urlsplit, urlunsplit

for url in urls:
    r1 = urlsplit(url)
    print( format_url(r1.geturl()) )
