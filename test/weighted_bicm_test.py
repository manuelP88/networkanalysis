import igraph as ig
import numpy as np
import bicm as b
import pandas as pd


def MOKUP_edge_list_with_duplicates():
    return [
        ('u_1', 'd_1'),
        ('u_1', 'd_2'),
        ('u_2', 'd_1'),
        ('u_3', 'd_1'),
        ('u_4', 'd_2'),
        ('u_5', 'd_3'),
        ('u_6', 'd_1'),
        ('u_1', 'd_1')
    ]

def to_key(user, domain):
    return str(user)+"_"+str(domain)

def from_edge_list_to_weighted_biadiacency_matrix(edge_list:list=None):

    v_rows = [itm[1] for itm in edge_list]
    v_cols = [itm[0] for itm in edge_list]

    enum_v_rows = list(enumerate(set(v_rows)))
    enum_v_cols = list(enumerate(set(v_cols)))

    from_enc_to_rows_v = {enc[0]: enc[1] for enc in enum_v_rows}
    from_enc_to_cols_v = {enc[0]: enc[1] for enc in enum_v_cols}

    from_rows_v_to_enc = {enc[1]: enc[0] for enc in enum_v_rows}
    from_cols_v_to_enc = {enc[1]: enc[0] for enc in enum_v_cols}

    b_matrix = np.zeros((len(enum_v_rows), len(enum_v_cols)))

    pd_edgelist = pd.DataFrame({'cols':v_cols, 'rows':v_rows})

    gb = pd_edgelist.groupby(['cols', 'rows']).size().reset_index(name="count")
    for index, row in gb.iterrows():
        col_v, row_v, count_v = row['cols'], row['rows'], row['count']
        enc_col_v = from_cols_v_to_enc[col_v] # giusta ecc se dato non trovato
        enc_row_v = from_rows_v_to_enc[row_v] # giusta ecc se dato non trovato
        b_matrix[enc_row_v][enc_col_v] = count_v

    return b_matrix, from_enc_to_rows_v, from_enc_to_cols_v



b_matrix, from_enc_to_rows_v, from_enc_to_cols_v = from_edge_list_to_weighted_biadiacency_matrix(
    MOKUP_edge_list_with_duplicates()
)

bg = b.BipartiteGraph(biadjacency=b_matrix)
bg.solve_tool(model='biwcm_c')
_m = bg.get_validated_matrix(significance=0.01, validation_method='fdr')

print("Rows Encoding: {}".format(from_enc_to_rows_v))
print("Cols Encoding: {}".format(from_enc_to_cols_v))
print("Input Matrix (dtype={}): {}".format(b_matrix.dtype, b_matrix))
print("Output Matrix (dtype={}): {}".format(_m.dtype, _m))

