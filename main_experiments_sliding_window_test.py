import argparse
import datetime
import core.utils as u
import core.business_logic as bl
import os

from core.performance import MetricsCalculator

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("-in", dest="in_path", default='./data/vaccino_final_old.csv')
    parser.add_argument("-out", dest="out_path", default='./data/out/')
    parser.add_argument("-from", dest="from_date", default='2021-09-01')
    parser.add_argument("-to", dest="to_date", default='2021-09-25')

    args = parser.parse_args()

    csv_in_path = args.in_path
    out = args.out_path
    from_date:datetime.date = datetime.date.fromisoformat(u.require_not_None(args.from_date))
    to_date:datetime.date = datetime.date.fromisoformat(u.require_not_None(args.to_date))

    settings = [
        #bl.SWExperimentalSettings(csv_in_path, out, from_date, to_date, 1, 3),
        #bl.SWExperimentalSettings(csv_in_path, out, from_date, to_date, 1, 6),
        #bl.SWExperimentalSettings(csv_in_path, out, from_date, to_date, 1, 9),
        #bl.SWExperimentalSettings(csv_in_path, out, from_date, to_date, 1, 12),
        #bl.SWExperimentalSettings(csv_in_path, out, from_date, to_date, 2, 3),
        #bl.SWExperimentalSettings(csv_in_path, out, from_date, to_date, 2, 6),
        #bl.SWExperimentalSettings(csv_in_path, out, from_date, to_date, 2, 9),
        #bl.SWExperimentalSettings(csv_in_path, out, from_date, to_date, 2, 12)
        bl.SWExperimentalSettings(csv_in_path, out, from_date, to_date, 5, 24, ignore_unique_urls=True)
    ]

    for s in settings:
        bl.util_execute_sliding_window_analysis( s )
        exporter: u.FileExporterManager = u.FileExporterManager( os.path.join(s.experiment_dir, "performance.csv"), ';')
        calc: MetricsCalculator = MetricsCalculator.compute_sliding_window(os.path.join(s.experiment_dir, "users_community_data.csv"), s.from_date, s.to_date, s.window_shift_days, s.window_size_days)
        calc.append_only_contiguos_windows_metrics(exporter, s)
        exporter.flush()