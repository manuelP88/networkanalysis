import pandas as pd
import core.utils as u
import datetime
import argparse
from core.performance import MetricsCalculator




if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("-in", dest="in_path", default='/Users/manuel/Documents/Work/ntw_img/sliding_window_20210901_20211005_7_size_1_shift/sliding_window_20210901_20211005_7_size_1_shift_users_community_data.csv')
    parser.add_argument("-out", dest="out_path", default='/Users/manuel/Documents/Work/ntw_img/')
    parser.add_argument("-from", dest="from_date", default='2021-09-01')
    parser.add_argument("-to", dest="to_date", default='2021-10-05')
    parser.add_argument("-window_shift", dest="window_shift_days", default=1)
    parser.add_argument("-window_size", dest="window_size_days", default=7)
    args = parser.parse_args()

    csv_in_path = args.in_path
    out: str = args.out_path
    window_shift: str = args.window_shift_days
    window_size: str = args.window_size_days
    from_date:datetime.date = datetime.date.fromisoformat(u.require_not_None(args.from_date))
    to_date:datetime.date = datetime.date.fromisoformat(u.require_not_None(args.to_date))

    exporter:u.FileExporterManager = u.FileExporterManager("/Users/manuel/Documents/Work/ntw_img/out2.csv", ';')

    calc: MetricsCalculator = MetricsCalculator.compute_sliding_window(csv_in_path, from_date, to_date, window_shift, window_size)
    calc.append_only_contiguos_windows_metrics(exporter)
    exporter.flush()











