import argparse
import datetime
import core.utils as u
import core.business_logic as bl
import pandas as pd
import core.utils_data_manipulation as dm
import igraph as ig
import os
from _data import user_in_echo_chambers

class MyLoader2(bl.DatasetLoader):
    def __init__(self, csv_in_path:str=None, interval=None, filter_wout_url=False):
        u.require_not_None(csv_in_path)
        u.require_not_None(interval)

        usr_graph_nodes_path = "G:\\Il mio Drive\\Colab Notebooks\\datasets\\network\\20210901_20210924_lcc_vaccine_usr_nodes.csv"
        url_graph_nodes_path = "G:\\Il mio Drive\\Colab Notebooks\\datasets\\network\\20210901_20210924_lcc_vaccine_url_nodes_orientation.csv"
        discoursive_comm_nodes_path = "G:\\Il mio Drive\\Colab Notebooks\\datasets\\network\\label_propagation_retweet_network_nodes.csv"

        df: pd.DataFrame = pd.read_csv(csv_in_path, dtype=str)

        #print( "ALERT DEBUGGING MODE ON!" )
        #df = df.head(1000)

        print("Loaded: ", csv_in_path)
        df['created_at'] = pd.to_datetime(df['created_at'], format='%a %b %d %H:%M:%S %z %Y').dt.tz_localize(None)
        df['verified'] = df['verified'].map({'True': True, 'False': False})
        df['retweeted_verified'] = df['retweeted_verified'].map({'True': True, 'False': False})
        df = df.astype({"user_id": str, "retweeted_user_id": str})

        print( str(len(df.index)) )
        df = dm.load_manipulate_and_marge_users_info(
            df,
            usr_graph_nodes_path,
            url_graph_nodes_path,
            discoursive_comm_nodes_path,
            attributes_name=[
                "id",
                "created_at",
                "user_id",
                "verified",
                "retweeted_verified",
                "is_retweet",
                "is_reply",
                "is_quoted",
                "retweeted_user_id",
                "v_ver_comm_label",
                "v_comm_urls",
                "v_comm_usr",
                "link_url_to_expand",
                "resolved_domain",
                "Rating",
                "disc_comm_flag",
                "url_comm_flag",
                "usr_comm_flag",
                "v_comm_usr_source",
                "v_comm_usr_destin",
            ]
        )
        # FILTER OTHERS DC DIFFERENT FROM "FdI-L-Media"
        df = df.loc[(df["v_ver_comm_label"] == "FdI-L-Media")]  # & ((df["usr_comm_flag"] == "True"))]

        print("Filter out DC differents from FdI-L-Media")

        self.edge_prod: bl.RtNetworkEdgeList = bl.RtNetworkEdgeList(df, interval[0], interval[1], filter_wout_url=filter_wout_url)
        print( str(len( self.edge_prod.pure_retweets().index)) )

    def getEdgeListProd(self) -> bl.AbstractEdgeListProd:
        return self.edge_prod




if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("-incompl", dest="in_path_total", default='../../data/vaccino_total.csv')
    parser.add_argument("-out", dest="out_path", default='../../data/out/2021_vaccine_italy')
    parser.add_argument("-from", dest="from_date", default='2021-09-01')
    parser.add_argument("-to", dest="to_date", default='2021-09-24') #2021-10-06
    args = parser.parse_args()

    csv_in_path_total = args.in_path_total

    out = args.out_path
    from_date:datetime.date = datetime.date.fromisoformat(u.require_not_None(args.from_date))
    to_date:datetime.date = datetime.date.fromisoformat(u.require_not_None(args.to_date))
    total_from_date:datetime.date = datetime.date.fromisoformat(u.require_not_None("2021-09-01"))
    total_to_date:datetime.date = datetime.date.fromisoformat(u.require_not_None("2021-09-24"))

    from_date: datetime.date = datetime.date.fromisoformat(u.require_not_None(args.from_date))
    to_date: datetime.date = datetime.date.fromisoformat(u.require_not_None(args.to_date))


    bl.produce_aggregated_by_usrnec_rt_network(
        MyLoader2(csv_in_path_total, (from_date, to_date), filter_wout_url=False),
        focus_on_giant=False,
        experiment_output_dir=out,
        user_in_echo_chambers=user_in_echo_chambers
    )

