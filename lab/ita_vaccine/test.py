from scipy.sparse import coo_matrix
import scipy.sparse
from bicm import BipartiteGraph

file_name = "sparse_matrix.npz"
_matrix = scipy.sparse.load_npz(file_name)

# Compute projection
bg: BipartiteGraph = BipartiteGraph(biadjacency=_matrix)
bg.set_to_continuous()
bg.solve_tool(model='biwcm_c')
_validated_matrix = bg.get_validated_matrix(significance=0.01, validation_method='fdr')