import argparse
import datetime
import core.utils as u
import core.business_logic as bl
import pandas as pd
import core.utils_data_manipulation as dm
import igraph as ig
import os
from _data import user_in_echo_chambers

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("-incompl", dest="in_path_total", default='../../data/vaccino_total.csv')
    parser.add_argument("-out", dest="out_path", default='../../data/out/2021_vaccine_italy')
    parser.add_argument("-from", dest="from_date", default='2021-09-01')
    parser.add_argument("-to", dest="to_date", default='2021-09-24') #2021-10-06
    args = parser.parse_args()

    csv_in_path_total = args.in_path_total
    out = args.out_path
    from_date: datetime.date = datetime.date.fromisoformat(u.require_not_None(args.from_date))
    to_date: datetime.date = datetime.date.fromisoformat(u.require_not_None(args.to_date))


    bl.execute_weighted_domain_users_projection(
        csv_in_path_total,
        (from_date, to_date),
        experiment_output_dir=out,
        ignore_uniques_urls=False,
        lang=None)