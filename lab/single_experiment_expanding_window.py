import argparse
import datetime
import os

from core import utils as u
import core.business_logic as bl





if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("-in", dest="in_path", default='/Users/manuel/Documents/Work/vaccino.csv')
    parser.add_argument("-out", dest="out_path", default='/Users/manuel/Documents/Work/ntw_img/')
    parser.add_argument("-from", dest="from_date", default='2021-09-01')
    parser.add_argument("-to", dest="to_date", default='2021-09-05')
    parser.add_argument("-window_increment", dest="window_increment_days", default=1)
    args = parser.parse_args()

    csv_in_path = args.in_path
    out: str = args.out_path
    window_increment_days: int = args.window_increment_days

    from_date:datetime.date = datetime.date.fromisoformat(u.require_not_None(args.from_date))
    to_date:datetime.date = datetime.date.fromisoformat(u.require_not_None(args.to_date))

    bl.util_execute_expanding_window_analysis(csv_in_path, out, from_date, to_date, window_increment_days, ignore_uniques_urls=False)
