import argparse
import datetime
from core import utils as u
import core.business_logic as bl
import pandas as pd

from dataclasses import make_dataclass

Edge = make_dataclass("Edge", [
    ("link", str),
    ("user_id", str),
    ("is_retweet", bool),
    ("created_at", str),
])

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("-in", dest="in_path", default='../../data/vaccino_final.csv')
    parser.add_argument("-out", dest="out_path", default='../../data/out/vaccino_verbose_edge_list_2.csv')
    parser.add_argument("-from", dest="from_date", default='2021-09-01')
    parser.add_argument("-to", dest="to_date", default='2021-09-24')
    parser.add_argument("-window_shift", dest="window_shift_days", default=1)
    parser.add_argument("-window_size", dest="window_size_days", default=2)
    args = parser.parse_args()

    csv_in_path = args.in_path
    out: str = args.out_path
    window_shift: str = args.window_shift_days
    window_size: str = args.window_size_days
    from_date:datetime.date = datetime.date.fromisoformat(u.require_not_None(args.from_date))
    to_date:datetime.date = datetime.date.fromisoformat(u.require_not_None(args.to_date))

    analysis_type = "user/url"  # TODO "url" or "domain"

    df: pd.DataFrame = pd.read_csv(csv_in_path, dtype=str)
    print("Loaded: ", csv_in_path)
    df['created_at'] = pd.to_datetime(df['created_at'], format='%a %b %d %H:%M:%S %z %Y').dt.tz_localize(None)
    df['verified'] = df['verified'].map({'True': True, 'False': False})
    df['retweeted_verified'] = df['retweeted_verified'].map({'True': True, 'False': False})

    def item_builder(row):
        u.require_not_None(row)
        return [row['user_id'], row['is_retweet'], row['created_at']]


    edge_prod: bl.UserURLEdgeList = bl.UserURLEdgeList(df, from_date, to_date, ignore_uniques_urls=True)
    user_by_url = edge_prod.produce_verbose_adjacency_list_as_dict(analysis_type="user/url", item_builder=item_builder)

    future_export = []
    for itm_i in user_by_url.items():
        for item_j in itm_i[1]:
            future_export.append(Edge(itm_i[0], item_j[0], item_j[1], item_j[2]))

    df = pd.DataFrame(future_export)
    df.to_csv(out, index=False)
    print("output produced at "+str(out))

