import argparse
import datetime
import pickle

from core import utils as u
import core.business_logic as bl
import pandas as pd

from dataclasses import make_dataclass

Edge = make_dataclass("Edge", [
    ("ver_user_id", str),
    ("unv_user_id", str)
])

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("-in", dest="in_path", default='../../data/vaccino_total.csv')
    parser.add_argument("-out", dest="out_path", default='../../data/out/vaccino_ver_unv_edge_list_v4_20210901_20210925.csv')
    parser.add_argument("-outp", dest="outp_path", default='../../data/out/vaccino_ver_unv_adj_list_v4_20210901_20210925.pickle')
    parser.add_argument("-from", dest="from_date", default='2021-09-01')
    parser.add_argument("-to", dest="to_date", default='2021-09-25')

    args = parser.parse_args()

    csv_in_path = args.in_path
    out: str = args.out_path
    outp: str = args.outp_path

    from_date:datetime.date = datetime.date.fromisoformat(u.require_not_None(args.from_date))
    to_date:datetime.date = datetime.date.fromisoformat(u.require_not_None(args.to_date))

    df: pd.DataFrame = pd.read_csv(csv_in_path, dtype=str)
    print("Loaded: ", csv_in_path)
    df['created_at'] = pd.to_datetime(df['created_at'], format='%a %b %d %H:%M:%S %z %Y').dt.tz_localize(None)
    df['verified'] = df['verified'].map({'True': True, 'False': False})
    df['retweeted_verified'] = df['retweeted_verified'].map({'True': True, 'False': False})

    def item_builder(row):
        u.require_not_None(row)
        return [row['user_id'], row['is_retweet'], row['created_at']]

    edge_prod: bl.UserVerNotVerEdgeList = bl.UserVerNotVerEdgeList(df, from_date, to_date)
    unverified_by_verified = edge_prod.produce_adjacency_list_as_dict()

    with open(outp, 'wb') as f:
        pickle.dump(unverified_by_verified , f)

    future_export = []
    for itm_i in unverified_by_verified.items():
        for item_j in itm_i[1]:
            future_export.append(Edge(itm_i[0], item_j))

    df = pd.DataFrame(future_export)
    df.to_csv(out, index=False)
    print("output produced at "+str(out))

