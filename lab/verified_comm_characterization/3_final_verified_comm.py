import pandas as pd
import tweepy

import core.utils as u
from typing import List
import numpy as np



if __name__ == '__main__':
    """ #VACCINE 
    csv_in_1 = "../../data/verified_user_comm_tagged.csv"
    #prende csv con TAG comm (header : v_comm,TAG)
    csv_in_2 = "../../data/verified_comm_characterized.csv"
    csv_out = "../../data/20210901_20211006_verified_usr_nodes_comm_tagged.csv"
    """
    """2020 US election
    prefix = "20201027_20201103"
    csv_in_1 = "C:\\Users\\manue\\PycharmProjects\\networkanalysis\\data\\out\\2020_us_election\\2_"+prefix+"_verified_user_nodes_objects_tagged_final.csv"
    #prende csv con TAG comm (header : v_comm,TAG)
    csv_in_2 = "C:\\Users\\manue\\PycharmProjects\\networkanalysis\\data\\out\\2020_us_election\\2_"+prefix+"_verified_user_nodes_objects_characterized.csv"
    csv_out = "C:\\Users\\manue\\PycharmProjects\\networkanalysis\\data\\out\\2020_us_election\\3_"+prefix+"_verified_usr_nodes_comm_tagged.csv"
    """

    # VACCINE reproducibility
    # Questo setting è stato introidotto per riprodurre gli esperimenti partendo da un intervallo
    # 20210901 - 20210924 della proiezione ver/unv .
    # Il diverso intervallo NON dovrebbe avere effetto sui risultati
    csv_in_1 = "../../data/verified_user_comm_tagged.csv"
    #prende csv con TAG comm (header : v_comm,TAG)
    csv_in_2 = "../../data/verified_comm_characterized.csv"
    csv_out = "../../data/out/2021_vaccine_italy_reproducibility/20210901_20211006_verified_usr_nodes_comm_tagged.csv"

    users_nodes = pd.read_csv(csv_in_1, dtype=str)
    users_nodes = users_nodes.rename({"TAG" : "usr_TAG", "v_comm" : "ver_comm", "v_name":"user_id", "screen_name" : "name"}, axis=1)
    comm_tags = pd.read_csv(csv_in_2, dtype=str)
    comm_tags = comm_tags.rename({"TAG" : "ver_comm_TAG", "v_comm" : "ver_comm"}, axis=1)

    res = pd.merge(
        users_nodes,
        comm_tags,
        how="left",
        left_on=["ver_comm"],
        right_on=["ver_comm"],
        left_index=False,
        right_index=False,
        sort=True,
        copy=True,
        indicator=False,
        validate="many_to_one"
    )

    res = res[['Id','Label','v_verified','user_id','name','ver_comm','usr_TAG','ver_comm_TAG']]
    u.remove_file_if_exist(csv_out)

    print( users_nodes.info() )
    print( comm_tags.info() )
    print( res.info() )
    print( res )

    u.remove_file_if_exist(csv_out)
    res.to_csv( csv_out, index=False)


