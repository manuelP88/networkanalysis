import pandas as pd
import core.utils as u

if __name__ == '__main__':
    csv_in = "../data/verified_user_comm_tagged.csv"
    csv_out_missing = "../data/verified_user_objects_missing.csv"
    csv_out_characterization = "../data/verified_user_comm_characterization.csv"

    users_nodes = pd.read_csv(csv_in, dtype=str)

    g = users_nodes.groupby(['v_comm', 'TAG']).size().reset_index(name="count")
    g = g.sort_values(by="count", ascending=False)
    g = g.sort_values(by="v_comm", ascending=False)
    u.remove_file_if_exist(csv_out_characterization)
    g.to_csv( csv_out_characterization, index=False)