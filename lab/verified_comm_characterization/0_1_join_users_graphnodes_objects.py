import pandas as pd
import core.utils as u
from tqdm import tqdm
from dataclasses import make_dataclass


verifiedUserResult = make_dataclass("VerifiedUser", [
                                   ("user_id", str),
                                   ("screen_name", str),
                                   ("description", str)
                                   ])

def map_row_into_json(row=None, col_user_id:str=None, col_screen_name:str=None, col_user_desc:str=None):
    u.require_not_None(row)
    u.require_not_None(col_user_id)
    u.require_not_None(col_screen_name)
    u.require_not_None(col_user_desc)

    return verifiedUserResult(
        str(row[col_user_id]),
        str(row[col_screen_name]),
        str(row[col_user_desc]).replace("\n", "").replace(",", "")
    )


if __name__ == '__main__':
    csv_in_ver_details = "C:\\Users\\manue\\PycharmProjects\\networkanalysis\\data\\out\\2020_us_election\\0_verified_user_objects.csv"
    csv_in_nodes = "C:\\Users\\manue\\PycharmProjects\\networkanalysis\\data\\out\\2020_us_election\\20201027_20201103_verified_usr_nodes.csv"
    csv_out = "C:\\Users\\manue\\PycharmProjects\\networkanalysis\\data\\out\\2020_us_election\\1_20201027_20201103_verified_user_nodes_objects.csv"

    users_nodes = pd.read_csv(csv_in_nodes, dtype=str)
    users_details = pd.read_csv(csv_in_ver_details, dtype=str)

    res = pd.merge(
        users_nodes,
        users_details,
        how="left",
        left_on=["v_name"],
        right_on=["user_id"],
        left_index=False,
        right_index=False,
        sort=True,
        copy=True,
        indicator=False,
        validate="many_to_one"
    )

    res["TAG"] = "UNC"
    res = res.sort_values(by="Degree", ascending=False)
    u.remove_file_if_exist(csv_out)
    res.to_csv( csv_out, index=False)
