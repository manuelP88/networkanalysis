import pandas as pd
import core.utils as u
from tqdm import tqdm
from dataclasses import make_dataclass


verifiedUserResult = make_dataclass("VerifiedUser", [
                                   ("user_id", str),
                                   ("screen_name", str),
                                   ("description", str)
                                   ])

def map_row_into_json(row=None, col_user_id:str=None, col_screen_name:str=None, col_user_desc:str=None):
    u.require_not_None(row)
    u.require_not_None(col_user_id)
    u.require_not_None(col_screen_name)
    u.require_not_None(col_user_desc)

    return verifiedUserResult(
        str(row[col_user_id]),
        str(row[col_screen_name]),
        str(row[col_user_desc]).replace("\n", "").replace(",", "")
    )


if __name__ == '__main__':
    csv_in = "G:\\Il mio Drive\\Projects\\URL_Analysis\\dataset_analysis\\raw_dataset\\2020-USelection-swing-state.csv"
    csv_out = "C:\\Users\\manue\\PycharmProjects\\networkanalysis\\data\\out\\2020_us_election\\verified_user_objects.csv"

    u.remove_file_if_exist(csv_out)

    all_tweets = pd.read_csv(csv_in, dtype=str)
    all_tweets['verified'] = all_tweets['verified'].map({'True': True, 'False': False})
    all_tweets['retweeted_verified'] = all_tweets['retweeted_verified'].map({'True': True, 'False': False})
    print(all_tweets.info())

    only_verified_uniques:pd.DataFrame = all_tweets.loc[all_tweets["verified"]==True]
    only_verified_uniques = only_verified_uniques.drop_duplicates(subset=['user_id'], keep='first')

    only_retweeted_verified_uniques:pd.DataFrame = all_tweets.loc[all_tweets['retweeted_verified']==True]
    only_retweeted_verified_uniques = only_retweeted_verified_uniques.drop_duplicates(subset=['retweeted_user_id'], keep='first')

    print("#tweets", str( all_tweets['id'].nunique() ))
    print("#user_id", str( all_tweets['user_id'].nunique() ))
    print("#retweeted_user_id", str( all_tweets['retweeted_user_id'].nunique() ))
    print("#verified", str( only_verified_uniques['user_id'].nunique() ))
    print("#rt_verified", str( only_retweeted_verified_uniques['retweeted_user_id'].nunique() ))

    users_details:dict = {}

    for index, row in tqdm(only_verified_uniques.iterrows(), desc="", total=len(only_verified_uniques.index)):
        key = row["user_id"]
        if key not in users_details:
            users_details[key] = map_row_into_json(row,
                                                   col_user_id="user_id",
                                                   col_screen_name="screen_name",
                                                   col_user_desc="user_description_if_verified")

    for index, row in tqdm(only_retweeted_verified_uniques.iterrows(), desc="", total=len(only_retweeted_verified_uniques.index)):
        key = row["retweeted_user_id"]
        if key not in users_details:
            users_details[key] = map_row_into_json(row,
                                                   col_user_id="retweeted_user_id",
                                                   col_screen_name="retweeted_screen_name",
                                                   col_user_desc="retweeted_user_description_if_verified")

    future_insert = [u for u in users_details.values()]
    out = pd.DataFrame(future_insert)
    out.to_csv(csv_out, index=False)