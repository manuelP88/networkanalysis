import pandas as pd
import tweepy

import core.utils as u
from typing import List
import numpy as np

if __name__ == '__main__':
    csv_in = "../data/20210901_20211006_verified_usr_nodes.csv"
    csv_out = "../data/verified_user_objects.csv"

    u.remove_file_if_exist(csv_out)

    users_nodes = pd.read_csv(csv_in, dtype=str)
    print(users_nodes.info())
    user_ids = users_nodes["v_name"].tolist()
    user_objects:List[tweepy.User] = u.users_lookup( user_ids )
    user_objects_by_id:dict = {u.id_str:u for u in user_objects}
    users_nodes["name"] = users_nodes['v_name'].apply(lambda v_name: np.nan if v_name not in user_objects_by_id else user_objects_by_id[ v_name ].name)
    users_nodes["description"] = users_nodes['v_name'].apply(lambda v_name: np.nan if v_name not in user_objects_by_id else str(user_objects_by_id[ v_name ].description).replace("\n", "").replace(",", ""))
    users_nodes.to_csv( csv_out, index=False)