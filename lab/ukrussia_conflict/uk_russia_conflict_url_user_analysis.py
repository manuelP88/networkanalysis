import argparse
import datetime
import core.utils as u
import core.business_logic as bl

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    #parser.add_argument("-in", dest="in_path", default="G:\\Il mio Drive\\Projects\\URL_Analysis\\dataset_analysis\\raw_dataset\\2020-USelection-swing-state.csv")
    parser.add_argument("-in", dest="in_path", default="C:\\Users\\manue\\PycharmProjects\\networkanalysis\\data\\out\\russiaukraineconflict\\solve_url_inconsistency\\v1_final_russiaukraineconflict_20220325_20220506.csv")
    parser.add_argument("-out", dest="out_path", default="C:\\Users\\manue\\PycharmProjects\\networkanalysis\\data\\out\\russiaukraineconflict\\all")
    parser.add_argument("-from", dest="from_date", default='2022-05-12')#default='2022-02-24')
    parser.add_argument("-to", dest="to_date", default='2022-05-19')#default='2022-03-24')
    args = parser.parse_args()

    prefix = "20220324_20220407"
    csv_ver_comm_tagged = None#"C:\\Users\\manue\\PycharmProjects\\networkanalysis\\data\\out\\2020_us_election\\3_" + prefix + "_verified_usr_nodes_comm_tagged.csv"
    csv_in_path = args.in_path
    out = args.out_path
    from_date:datetime.date = datetime.date.fromisoformat(u.require_not_None(args.from_date))
    to_date:datetime.date = datetime.date.fromisoformat(u.require_not_None(args.to_date))

    bl.execute_analysis(csv_in_path=csv_in_path,
                        intervals=[[from_date, to_date]],
                        experiment_output_dir=out,
                        ignore_uniques_urls=True,
                        lang="en")
    """
    bl.execute_discussion_comm_analysis(
        csv_in_path,
        (from_date, to_date),
        out
    )"""

    """
    bl.execute_label_propagation_analysis_RTNetwork(
        csv_in_path,
        csv_ver_comm_tagged,
        (from_date, to_date),
        out
    )"""

    """
    bl.execute_label_propagation_analysis_VerUnvNetwork(
        csv_in_path,
        csv_ver_comm_tagged,
        (from_date, to_date),
        out
    )"""
