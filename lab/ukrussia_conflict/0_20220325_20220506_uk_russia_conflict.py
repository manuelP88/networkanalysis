import os.path

from core.url_short_long_consistency import solve_url_inconsistency

if __name__ == '__main__':

    path_to_data = "C:\\Users\\manue\\PycharmProjects\\networkanalysis\\data"
    base_path_out = os.path.join( path_to_data, "out", "russiaukraineconflict_20220325_20220506", "solve_url_inconsistency")
    path_vaccino_total = "G:\\Il mio Drive\\Projects\\URL_Analysis\\dataset_analysis\\raw_dataset\\russiaukraineconflict_20220325_20220506.csv"

    solve_url_inconsistency(path_vaccino_total, base_path_out)