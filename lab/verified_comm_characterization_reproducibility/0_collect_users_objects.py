import pandas as pd
import tweepy

import core.utils as u
from typing import List
import numpy as np

if __name__ == '__main__':
    csv_in_1 = "C:\\Users\\manue\\PycharmProjects\\networkanalysis\\data\\out\\2021_vaccine_italy_reproducibility\\sliding_window_20210901_20210924_23_size_5_shift\\20210901_20210924_verified_reproducibility.csv"#../data/20210901_20211006_verified_usr_nodes.csv"
    csv_in_2 = "C:\\Users\\manue\\PycharmProjects\\networkanalysis\\data\\out\\2021_vaccine_italy_reproducibility\\sliding_window_20210901_20210924_23_size_5_shift\\20210901_20211006_verified_usr_nodes_comm_tagged_reproducibility.csv"#../data/20210901_20211006_verified_usr_nodes.csv"
    csv_out = "C:\\Users\\manue\\PycharmProjects\\networkanalysis\\data\\out\\2021_vaccine_italy_reproducibility\\sliding_window_20210901_20210924_23_size_5_shift\\out\\verified_user_comm_tagged.csv"

    users_nodes = pd.read_csv(csv_in_1, dtype=str)
    users_nodes = users_nodes[["v_name", "v_comm"]]
    users_nodes = users_nodes.rename(
        {"v_comm": "ver_comm_new", "v_name": "user_id"}, axis=1)

    comm_tags = pd.read_csv(csv_in_2, dtype=str)
    comm_tags = comm_tags[["user_id","ver_comm","name","usr_TAG","ver_comm_TAG"]]
    comm_tags = comm_tags.rename({"ver_comm" : "ver_comm_old", "ver_comm_TAG": "ver_comm_TAG_old", }, axis=1)

    res = pd.merge(
        users_nodes,
        comm_tags,
        how="left",
        left_on=["user_id"],
        right_on=["user_id"],
        left_index=False,
        right_index=False,
        sort=True,
        copy=True,
        indicator=False,
        validate="one_to_one"
    )

    print(res.info())

    res = res[['user_id', 'name', 'usr_TAG', 'ver_comm_old', 'ver_comm_TAG_old', 'ver_comm_new']]
    u.remove_file_if_exist(csv_out)

    print(users_nodes.info())
    print(comm_tags.info())
    print(res.info())
    print(res)

    u.remove_file_if_exist(csv_out)
    res.to_csv(csv_out, index=False)