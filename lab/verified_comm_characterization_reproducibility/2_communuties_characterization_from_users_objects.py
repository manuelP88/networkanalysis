import pandas as pd
import tweepy

import core.utils as u
from typing import List
import numpy as np

class Tagger(object):
    def __init__(self):
        pass

    def find_TAG(self, row=None):
        raise Exception("method not implemented yet")

class TaggerVaccino(Tagger):
    def __init__(self):
        pass

    def find_TAG(self, row=None):
        desc = row["description"]
        if desc is None: return ""
        if desc != "UNC": return desc
        if not isinstance(desc,str): return "UNC"

        if "pd" in desc.lower():
            return "PD"
        elif "partito democratico" in desc.lower():
            return "PD"
        elif "lega" in desc.lower():
            return "Lega"
        elif "salvini" in desc.lower():
            return "Lega"
        elif "m5s" in desc.lower():
            return "M5S"
        elif "mov5stelle" in desc.lower():
            return "M5S"
        elif "movimento 5 stelle" in desc.lower():
            return "M5S"
        elif "movimento cinque stelle" in desc.lower():
            return "M5S"
        elif "viva" in desc.lower():
            return "ItaliaViva"
        elif "fratelliditalia" in desc.lower():
            return "FratelliDItalia"
        elif "fratelli d’italia" in desc.lower():
            return "FratelliDItalia"
        elif "forza" in desc.lower():
            return "ForzaItalia"
        elif "tg" in desc.lower():
            return "Media"
        elif "giorn" in desc.lower():
            return "Media"
        elif "journalist" in desc.lower():
            return "Media"
        elif "quotidiano" in desc.lower():
            return "Media"
        elif "notiz" in desc.lower():
            return "Media"
        elif "media" in desc.lower():
            return "Media"
        elif "radio" in desc.lower():
            return "Media"
        else:
            return "UNC"

class TaggerUsElection(Tagger):
    def __init__(self):
        pass

    def find_TAG(self, row=None):
        desc = row["description"]
        tag = row["TAG"]

        #tag already assigned
        if tag != "UNC": return tag
        if desc is None or desc=="" : return "UNC"
        if not isinstance(desc, str): return "UNC"

        if "trump" in desc.lower():
            return "Rep"
        elif "biden" in desc.lower():
            return "Dem"
        elif "democrat" in desc.lower():
            return "Dem"
        elif "republican" in desc.lower():
            return "Rep"
        elif "conservative" in desc.lower():
            return "Conservative"
        elif "journalist" in desc.lower():
            return "Journalist"
        elif "reporter" in desc.lower():
            return "Journalist"
        else:
            return "UNC"

if __name__ == '__main__':

    base = "C:\\Users\\manue\\PycharmProjects\\networkanalysis\\data\\out\\2021_vaccine_italy_reproducibility\\sliding_window_20210901_20210924_23_size_5_shift\\out\\"#../data/20210901_20211006_verified_usr_nodes.csv"

    csv_in = base + "verified_user_comm_characterization.csv"
    csv_out_final = base + "2_verified_user_nodes_objects_tagged_final.csv"
    csv_out_tagged = base + "2_verified_user_nodes_objects_tagged.csv"
    csv_out_missing = base + "2_verified_user_nodes_objects_missing.csv"
    csv_out_characterization = base + "2_verified_user_nodes_objects_characterization.csv"
    csv_out_characterization = base + "3_verified_user_comm_characterization.csv"

    tagger = TaggerVaccino()
    min_degree = 0
    print(csv_in)
    users_nodes = pd.read_csv(csv_in, dtype=str)
    """    users_nodes["TAG"] = users_nodes["TAG"].astype(dtype=str)
    for i, row in users_nodes.iterrows(): users_nodes.at[i, 'TAG'] = tagger.find_TAG(row)
    users_nodes["Degree"] = users_nodes["Degree"].astype(dtype=float)
    users_nodes = users_nodes.loc[ users_nodes["Degree"] > min_degree ]
    print(users_nodes.nunique())"""

    tag_name = "usr_TAG"
    v_comm_name = "ver_comm_new"
    g = users_nodes.groupby([v_comm_name, tag_name]).size().reset_index(name="count")
    g = g.sort_values(by="count", ascending=False)
    g = g.sort_values(by=v_comm_name, ascending=True)
    u.remove_file_if_exist(csv_out_characterization)
    g.to_csv(csv_out_characterization, index=False)

    u.remove_file_if_exist(csv_out_final)
    users_nodes = users_nodes.sort_values(by="Degree", ascending=False)
    users_nodes.to_csv( csv_out_final, index=False)

    missing = users_nodes.loc[ users_nodes["TAG"]=="UNC" ]
    missing = missing.sort_values(by="Degree", ascending=False)
    missing.to_csv( csv_out_missing, index=False)
    print("Actual missing : ",str(len(missing.index)))

    tagged = users_nodes.loc[ users_nodes[tag_name]!="UNC" ]
    tagged = tagged.sort_values(by="Degree", ascending=False)
    tagged.to_csv( csv_out_tagged, index=False)
    print("Actual tagged : ",str(len(tagged.index)))

    g = users_nodes.groupby([v_comm_name, tag_name]).size().reset_index(name="count")
    g = g.sort_values(by="count", ascending=False)
    g = g.sort_values(by=v_comm_name, ascending=True)
    u.remove_file_if_exist(csv_out_characterization)
    g.to_csv(csv_out_characterization, index=False)




