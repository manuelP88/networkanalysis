import pandas as pd
import core.utils as u

if __name__ == '__main__':
    base = "C:\\Users\\manue\\PycharmProjects\\networkanalysis\\data\\out\\2021_vaccine_italy_reproducibility\\sliding_window_20210901_20210924_23_size_5_shift\\out\\"#../data/20210901_20211006_verified_usr_nodes.csv"

    csv_in = base + "verified_user_comm_tagged.csv"
    csv_out_missing = base + "verified_user_objects_missing.csv"
    csv_out_characterization = base + "verified_user_comm_characterization.csv"

    users_nodes = pd.read_csv(csv_in, dtype=str)

    g = users_nodes.groupby(['ver_comm_new', 'usr_TAG']).size().reset_index(name="count")
    g = g.sort_values(by="count", ascending=False)
    g = g.sort_values(by="ver_comm_new", ascending=False)
    u.remove_file_if_exist(csv_out_characterization)
    g.to_csv( csv_out_characterization, index=False)