import pandas as pd


if __name__ == '__main__':
    in_csv = "C:\\Users\\manue\\PycharmProjects\\networkanalysis\\data\\out\\2021_vaccine_italy_reproducibility\\sliding_window_20210901_20210924_23_size_5_shift\\out\\verified_user_comm_tagged.csv"
    out_csv = "C:\\Users\\manue\\PycharmProjects\\networkanalysis\\data\\out\\2021_vaccine_italy_reproducibility\\sliding_window_20210901_20210924_23_size_5_shift\\out\\verified_user_comm_tagged_final.csv"

    users_nodes:pd.DataFrame = pd.read_csv(in_csv, dtype=str)
    print(users_nodes.info())

    null_values = users_nodes.loc[ users_nodes["ver_comm_old"].isnull() ]
    print("Count Nan values: {} / {} ".format( len( null_values.index), len( users_nodes.index) ) )

    inconsistencies = []
    inconsistencies_ids = []
    map:dict = {}
    for idx, row in users_nodes.iterrows():
        key = row["ver_comm_old"]
        value = row["ver_comm_new"]
        if key not in map:
            map[key] = [value]

        if value not in map[key]:
            inconsistencies.append( row.tolist() )
            inconsistencies_ids.append( row["user_id"] )

    print("OLD : NEW community ID map")
    print( map )
    print("\nInconsistencies {}".format( len(inconsistencies) ))

    print( inconsistencies )

    results = users_nodes.loc[users_nodes["ver_comm_new"]=="6"]
    #results.to_csv( out_csv, index=False)

    print(results)

