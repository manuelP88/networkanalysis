import pandas as pd
from core.articles_extraction import sequential_scrape_titles_from_urls
import os
import datetime
import numpy as np

def _filter_by_date_interval(df: pd.DataFrame = None, from_date: datetime.date = None,
                             to_date: datetime.date = None):
     _from_ = np.datetime64( from_date )
     _to_ = np.datetime64( to_date )

     df_tmp = df.loc[(df['created_at'] >= _from_)]
     df_tmp = df_tmp.loc[(df['created_at'] <= _to_)]
     return df_tmp

def scrape_articles_titles_from_urls(urls, out_csv):

     urls = set( urls )

     if os.path.exists(out_csv):
          print("Starting URLs: " + str(len(urls)))
          df_out_old = pd.read_csv(out_csv, sep="§", engine='python')
          urls_old = set(df_out_old['short_url'].unique())
          urls = urls - urls_old
          print("Filtered URLs: " + str(len(urls)))

     if len( list(urls) ) > 0:

          future_insert, errors = sequential_scrape_titles_from_urls(list(urls))
          print( str(errors) )

          if len(future_insert) > 0:
               out = pd.DataFrame(future_insert)

               if os.path.exists(out_csv):
                    out_to_update = pd.read_csv(out_csv, sep="§", engine='python')
                    out = pd.concat([out_to_update, out])
                    os.remove(out_csv)

               out.to_csv(out_csv, sep="§")


def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i:i + n]

if __name__ == '__main__':
     base_url = "../data/topics_extraction/"
     in_csv = os.path.join(base_url, "urls_for_topic_extraction.csv")
     #in_csv = os.path.join(base_url, "test.csv")
     out_csv_only_titles = os.path.join(base_url, "out", "only_titles.csv")
     out_csv_input_with_titles = os.path.join(base_url, "out", "input_plus_titles.csv")

     from_date = '2021-09-01'
     to_date = '2021-09-24'
     chunk_size = 200
     df: pd.DataFrame = pd.read_csv(in_csv, dtype=str)
     df = df[df.link_url_to_expand.notnull()]
     #df['created_at'] = pd.to_datetime(df['created_at'], format='%a %b %d %H:%M:%S %z %Y').dt.tz_localize(None)
     #df = _filter_by_date_interval(df, from_date, to_date)

     urls = df['link_url_to_expand'].unique().tolist()

     for idx, urls_chunk in enumerate(list(chunks(urls, chunk_size))):
          print("******** chunk ",idx," ********")
          scrape_articles_titles_from_urls(urls_chunk, out_csv_only_titles)

     print("marging to input...")

     df_only_titles: pd.DataFrame = pd.read_csv(out_csv_only_titles, dtype=str, sep='§')

     view = pd.merge(
          df,
          df_only_titles,
          how="left",
          left_on=["link_url_to_expand"],
          right_on=["short_url"],
          left_index=False,
          right_index=False,
          sort=True,
          copy=True,
          indicator=False,
          validate="many_to_one"
     )

     view.to_csv(out_csv_input_with_titles, sep="§", index=False)
     print("saving OK --> " + out_csv_input_with_titles)


