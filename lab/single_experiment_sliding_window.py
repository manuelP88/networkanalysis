import argparse
import datetime
from core import utils as u
import core.business_logic as bl


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("-in", dest="in_path", default='..data/vaccino.csv')
    parser.add_argument("-out", dest="out_path", default='..data/out/')
    parser.add_argument("-from", dest="from_date", default='2021-09-15')
    parser.add_argument("-to", dest="to_date", default='2021-09-22')
    parser.add_argument("-window_shift", dest="window_shift_days", default=1)
    parser.add_argument("-window_size", dest="window_size_days", default=2)
    args = parser.parse_args()

    csv_in_path = args.in_path
    out: str = args.out_path
    window_shift: str = args.window_shift_days
    window_size: str = args.window_size_days
    from_date:datetime.date = datetime.date.fromisoformat(u.require_not_None(args.from_date))
    to_date:datetime.date = datetime.date.fromisoformat(u.require_not_None(args.to_date))

    bl.util_execute_sliding_window_analysis(
        bl.SWExperimentalSettings(csv_in_path, out, from_date, to_date, window_shift, window_size))


