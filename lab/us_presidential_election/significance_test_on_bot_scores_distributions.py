from typing import List
import datetime

from pandas import to_datetime
import matplotlib.pyplot as plt

import pandas as pd
import numpy as np
import random
import os
import scipy.stats
import core.utils as u

states = ['arizona', 'florida', 'michigan', 'pennsylvania', 'newjersey', 'indiana', 'washington', 'louisiana']
states_swing = ['arizona', 'florida', 'michigan', 'pennsylvania']
states_safe = ['newjersey', 'indiana', 'washington', 'louisiana']
rating_values = ['T', 'N', 'UNC', 'P']
url_reliability_colors= {'T': 'royalblue', 'N': 'r', 'S': 'm', 'P': 'aquamarine',  'UNC': 'silver' }

class TweetsFilterUtils(object):

    def __init__(self, df: pd.DataFrame = None, from_date: str = None, to_date: str = None):
        self._df: pd.DataFrame = self._filter_by_date_interval(df, from_date, to_date)

    def _filter_by_date_interval(self, df: pd.DataFrame=None, from_date: str = None, to_date: str = None):
        u.require_not_None(df)
        if from_date is None and to_date is None:
          return df

        _from_ = np.datetime64(datetime.date.fromisoformat(u.require_not_None(from_date)))
        _to_ = np.datetime64(datetime.date.fromisoformat(u.require_not_None(to_date)))

        df_tmp = df.loc[(df['created_at'] >= _from_)]
        df_tmp = df_tmp.loc[(df['created_at'] <= _to_)]
        return df_tmp

    def df(self) -> pd.DataFrame:
        return u.require_not_None(self._df)

    def pure_tweets(self) -> pd.DataFrame:
        return self.df().loc[(self.df().is_retweet == 'False') & (self.df().is_reply == 'False') & (self.df().is_quoted == 'False')]

    def pure_retweets(self) -> pd.DataFrame:
        return self.df().loc[(self.df().is_retweet == 'True') & (self.df().is_reply == 'False') & (self.df().is_quoted == 'False')]

    def pure_replies(self) -> pd.DataFrame:
        return self.df().loc[(self.df().is_retweet == 'False') & (self.df().is_reply == 'True') & (self.df().is_quoted == 'False')]

    def quoted_retweets(self) -> pd.DataFrame:
        return self.df().loc[(self.df().is_retweet == 'True') & (self.df().is_reply == 'False') & (self.df().is_quoted == 'True')]

    def quoted_tweets(self) -> pd.DataFrame:
        return self.df().loc[(self.df().is_retweet == 'False') & (self.df().is_reply == 'False') & (self.df().is_quoted == 'True')]

    def quoted_replies(self) -> pd.DataFrame:
        return self.df().loc[(self.df().is_retweet == 'False') & (self.df().is_reply == 'True') & (self.df().is_quoted == 'True')]


def url_reputability_distribution(df, col_rating_name="Rating", TAG=None) -> pd.DataFrame:
    rating_distr: pd.DataFrame = df.groupby([col_rating_name]).size().sort_values().reset_index(name="count")
    rating_distr_transposed: pd.DataFrame = rating_distr.T
    rating_distr_transposed.columns = rating_distr_transposed.iloc[0]
    rating_distr_transposed = rating_distr_transposed.drop(rating_distr_transposed.index[0])

    if TAG is not None: rating_distr_transposed["TAG"] = TAG

    return rating_distr_transposed

if __name__=="__main__":
    from_date = '2020-10-27'
    to_date = '2020-11-04'

    # dataset_path = "G:\\Il mio Drive\\Projects\\URL_Analysis\\dataset_analysis\\raw_dataset\\2020-USelection-swing-state.csv"
    dataset_path = "C:\\Users\\manue\\PycharmProjects\\networkanalysis\\data\\out\\2020_us_election\\solve_url_inconsistency\\v1_final_2020-USelection-swing-state.csv"
    url_community_path = "C:\\Users\\manue\\Dropbox\\us_election_network_analysis\\url_communities_network_nodes.csv"
    path_user_political_comm = "G:\\Il mio Drive\\Colab Notebooks\\datasets\\us_election\\label_propagation_retweet_network_nodes.csv"
    path_user_botscores = "C:\\Users\\manue\\Dropbox\\us_election_network_analysis\\botometer_scores\\20201027_20201103_bot_scores.csv"

    df: pd.DataFrame = pd.read_csv(dataset_path, dtype=str)

    df['created_at'] = pd.to_datetime(df['created_at'], format='%a %b %d %H:%M:%S %z %Y').dt.tz_localize(None)
    df['day'] = df['created_at'].dt.date
    df['trump'] = df['trump'].map({'True': True, 'False': False})
    df['biden'] = df['biden'].map({'True': True, 'False': False})
    df['arizona'] = df['arizona'].map({'True': True, 'False': False})
    df['florida'] = df['florida'].map({'True': True, 'False': False})
    df['michigan'] = df['michigan'].map({'True': True, 'False': False})
    df['pennsylvania'] = df['pennsylvania'].map({'True': True, 'False': False})
    df['newjersey'] = df['newjersey'].map({'True': True, 'False': False})
    df['indiana'] = df['indiana'].map({'True': True, 'False': False})
    df['washington'] = df['washington'].map({'True': True, 'False': False})
    df['louisiana'] = df['louisiana'].map({'True': True, 'False': False})

    tweets: TweetsFilterUtils = TweetsFilterUtils(df, from_date, to_date)

    # --------------------------------
    # JOIN WITH DICOURSIVE COMMUNITIES
    # --------------------------------

    user_nodes_pol_comm = pd.read_csv(path_user_political_comm, dtype=str)
    user_nodes_pol_comm = user_nodes_pol_comm[["v_name", "v_usr_tag", "v_ver_comm",
                                               "v_ver_comm_prop", "v_ver_comm_label", "Degree"]]

    total_view = pd.merge(
        tweets.df(),
        user_nodes_pol_comm,
        how="left",
        left_on=["user_id"],
        right_on=["v_name"],
        left_index=False,
        right_index=False,
        sort=True,
        copy=True,
        indicator=False,
        validate="many_to_one"
    )

    # --------------------------
    # JOIN WITH URLs COMMUNITIES
    # --------------------------

    url_community: pd.DataFrame = pd.read_csv(url_community_path, dtype=str)

    url_community = url_community.rename(columns={
        "v_comm": "v_comm_urls",
        "v_name": "short_url",
        "Degree": "Degree_url_comm"
    })

    total_view = pd.merge(
        total_view,
        url_community,
        how="left",
        left_on=["link_url_to_expand"],
        right_on=["short_url"],
        left_index=False,
        right_index=False,
        sort=True,
        copy=True,
        indicator=False,
        validate="many_to_one"
    )

    # --------------------------
    # JOIN WITH BOT SCORES
    # --------------------------

    user_botscores = pd.read_csv(path_user_botscores, dtype=str)
    user_botscores = user_botscores[["user_id", "bot_score"]]
    user_botscores['user_id'] = user_botscores['user_id'].astype('str')
    user_botscores['bot_score'] = user_botscores['bot_score'].astype('float')

    total_view = pd.merge(
        total_view,
        user_botscores,
        how="left",
        left_on=["user_id"],
        right_on=["user_id"],
        left_index=False,
        right_index=False,
        sort=True,
        copy=True,
        indicator=False,
        validate="many_to_one"
    )

    ### PRE - PROCESSING

    total_view['Degree'] = total_view['Degree'].fillna("0")
    total_view['Degree'] = total_view['Degree'].astype('int')
    total_view['Degree_url_comm'] = total_view['Degree_url_comm'].fillna("0")
    total_view['Degree_url_comm'] = total_view['Degree_url_comm'].astype('int')
    total_view['user_id'] = total_view['user_id'].astype('str')
    total_view['retweeted_user_id'] = total_view['retweeted_user_id'].astype('str')
    total_view['bot_score'] = total_view['bot_score'].fillna("-1")
    total_view['bot_score'] = total_view['bot_score'].astype('float')

    ### PRE - FILTERING

    # consider only en tweets
    total_view = total_view.loc[total_view.lang == "en"]
    # consider only tweet with ONLY ONE state in tweet corpus
    total_view['numb_states'] = total_view[states].sum(axis=1)
    total_view = total_view.loc[total_view.numb_states == 1]

    # consider only the main political communities 0 and 1
    comm_view = total_view.loc[
        ((total_view.v_ver_comm_prop == "1") | (total_view.v_ver_comm_prop == "0")) & (total_view.bot_score >= 0)]

    total_view: TweetsFilterUtils = TweetsFilterUtils(total_view, from_date, to_date)
    comm_view: TweetsFilterUtils = TweetsFilterUtils(comm_view, from_date, to_date)

    comm_view_Rep = comm_view.df().loc[comm_view.df()["v_ver_comm_prop"] == "0"]
    comm_view_Rep_Dem_Journ = comm_view.df().loc[comm_view.df()["v_ver_comm_prop"] == "1"]

    comm_view_Rep = TweetsFilterUtils(comm_view_Rep, from_date, to_date)
    comm_view_Rep_Dem_Journ = TweetsFilterUtils(comm_view_Rep_Dem_Journ, from_date, to_date)

    total_url_distribution = url_reputability_distribution(total_view.df(), "Rating", "Dataset")
    print( total_url_distribution )