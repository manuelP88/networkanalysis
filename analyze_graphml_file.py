import igraph as ig
import core.utils as u
import os
import datetime as dt

def export(projection_graph : ig.Graph=None, exporter: u.GraphExporterManager=None):
    u.require_not_None( url_projection_graph )
    u.require_not_None( exporter )

    from_date:dt.date = dt.date(year=2020, month=9, day=1)
    to_date:dt.date = dt.date(year=2020, month=10, day=5)

    exporter.append_graph_export(projection_graph, from_date=from_date, to_date=to_date)
    exporter.flush()
    print("[export] CSV OK")


if __name__ == '__main__':
    graphml_url_file_path : str = "./data/graph/graph_url_20210901_20211005.graphml"
    graphml_usr_file_path : str = "./data/graph/graph_usr_20210901_20211005.graphml"
    output_file_dir : str = "./data/out/"

    print("loading... ", graphml_url_file_path)
    url_projection_graph : ig.Graph = ig.Graph.Read( graphml_url_file_path )
    print("[init] URL graph loaded")

    # Export URLs
    url_csv_out:str = os.path.join(output_file_dir, "url_community_data.csv")
    url_graph_exporter: u.GraphExporterManager = u.GraphExporterManager(output_file=url_csv_out, sep=",")
    export(projection_graph=url_projection_graph, exporter=url_graph_exporter)

    # USER
    print("loading... ", graphml_usr_file_path)
    usr_projection_graph : ig.Graph = ig.Graph.Read( graphml_usr_file_path )
    print("[init] USER graph loaded")

    # Export USERs
    usr_csv_out = os.path.join(output_file_dir, "users_community_data.csv")
    user_graph_exporter: u.GraphExporterManager = u.GraphExporterManager(output_file=usr_csv_out, sep=",")
    export(projection_graph=usr_projection_graph, exporter=user_graph_exporter)

    # Export PLOT
    #url_plot: u.PlotAdapter = u.PlotAdapter("../data/out/")
    #url_plot.plot_graph(img_name="url_comminities.pdf",
    #                    g=url_projection_graph,
    #                    comm_color=True,
    #                    plot_name_lambda=lambda v_list: [v["rating"] if v["rating"] != "UNC" else "" for v in v_list])
    #print("[export] URL PLOT OK : ")
    #user_plot: u.PlotAdapter = u.PlotAdapter("../data/out/")
    #user_plot.plot_graph(img_name="usr_comminities.pdf",
    #                     g=usr_projection_graph,
    #                     comm_color=True,
    #                     plot_name_lambda=None)
    #print("[export] USER PLOT OK : ")






