import argparse
import datetime
import core.utils as u
import core.business_logic as bl
import os

from core.performance import MetricsCalculator

if __name__ == '__main__':

    # VACCINE reproducibility
    # Questo setting è stato introidotto per riprodurre gli esperimenti partendo da un intervallo
    # 20210901 - 20210924 della proiezione ver/unv .
    # Il diverso intervallo NON dovrebbe avere effetto sui risultati

    parser = argparse.ArgumentParser()
    parser.add_argument("-in", dest="in_path", default='./data/vaccino_final.csv')
    parser.add_argument("-incompl", dest="in_path_total", default='./data/vaccino_total.csv')
    #parser.add_argument("-out", dest="out_path", default='./data/out/2021_vaccine_italy_reproducibility/sliding_window_20210901_20210924_23_size_5_shift/label_prop')
    parser.add_argument("-out", dest="out_path", default='./data/out/2021_vaccine_italy_reproducibility_3/')
    parser.add_argument("-from", dest="from_date", default='2021-09-01')
    parser.add_argument("-to", dest="to_date", default='2021-09-24')
    args = parser.parse_args()

    csv_in_path = args.in_path
    csv_in_path_total = args.in_path_total

    out = args.out_path
    from_date:datetime.date = datetime.date.fromisoformat(u.require_not_None(args.from_date))
    to_date:datetime.date = datetime.date.fromisoformat(u.require_not_None(args.to_date))
    total_from_date:datetime.date = datetime.date.fromisoformat(u.require_not_None("2021-09-01"))
    total_to_date:datetime.date = datetime.date.fromisoformat(u.require_not_None("2021-09-25"))

    #csv_ver_comm_tagged = "./data/20210901_20211006_verified_usr_nodes_comm_tagged.csv"
    csv_ver_comm_tagged = "./data/out/2021_vaccine_italy_reproducibility/sliding_window_20210901_20210924_23_size_5_shift/out/verified_user_comm_tagged.csv"

    settings = [
        bl.SWExperimentalSettings(csv_in_path, out, from_date, to_date, 5, 23,
                                  ignore_unique_urls=True,
                                  csv_in_path_total=csv_in_path_total,
                                  total_from_date=total_from_date,
                                  total_to_date=total_to_date,
                                  csv_ver_comm_tagged=csv_ver_comm_tagged)
    ]

    for s in settings:
        bl.util_execute_sliding_window_analysis( s )