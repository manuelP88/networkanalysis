from typing import List
import pandas as pd
import datetime
import os
import igraph as ig
import numpy as np
import tweepy

import core.plotting as p


def require_not_None(obj, message="Require not None") -> object:
    if obj is None:
        raise Exception(message)
    return obj


class PlotAdapter(object):

    def __init__(self, base_out_dir_path: str=None):
        self._base_out_dir_path = require_not_None( base_out_dir_path )

    def plot_graph(self, img_name: str = None, g: ig.Graph = None, comm_color: bool = True, plot_name_lambda=None):
        p.plot_graph( target=os.path.join(self._base_out_dir_path, img_name), g=g, comm_color=comm_color, plot_name_lambda=plot_name_lambda )

class ExporterManager(object):

    def __init__(self, output_file=None, sep=None):
        self._output_file = require_not_None(output_file)
        self._header = None
        self._sep = require_not_None(sep)
        self._future_write = []

    def output_file(self) -> str:
        return require_not_None(self._output_file)

    def _set_header_if_None(self, d:dict=None):
        require_not_None(d)
        if self._header is None:
            self._header = []
            for key in d.keys():
                self._header.append(key)


    def _append(self, rows:List[dict]=None):
        require_not_None(rows)

        for row in rows:
            self._set_header_if_None(row)
            tmp = []
            for column in self._header:
                tmp.append(row[column])  # giusta exc se key non esiste

            self._future_write.append(tmp)



    def append_and_flush(self, rows:List[dict]=None):
        self._append(rows)
        self.flush()


    def flush(self):
        if len(self._future_write)==0 : return
        if self._header is None : raise Exception("null header")
        import os
        add_header = not os.path.isfile(self._output_file)
        results_df = pd.DataFrame(self._future_write, columns=self._header)
        results_df.to_csv(self._output_file, mode='a', sep=self._sep, index=False, header=add_header)
        self._future_write = []


class FileExporterManager(ExporterManager):

    def __init__(self, output_file=None, sep=None):
        super().__init__(output_file, sep)

    def append(self, rows: List[dict] = None):
        self._append(rows)


class GraphExporterManager(ExporterManager):

    def __init__(self, output_file=None, sep=None):
        super().__init__(output_file, sep)

    def append_graph_export(self, g: ig.Graph = None, from_date: datetime.date = None, to_date: datetime.date = None):
        require_not_None(g)
        require_not_None(from_date)
        require_not_None(to_date)
        future_add: List[dict] = []
        for comm_id in np.unique(g.vs['comm']):
            for v in g.vs.select(comm=comm_id):
                vtx: ig.Vertex = v
                attrs = vtx.attributes()
                attrs["degree"] = vtx.degree()
                attrs["pagerank"] = vtx.pagerank()
                attrs["eccentricity"] = vtx.eccentricity()
                attrs["betweenness"] = vtx.betweenness()
                attrs["from_date"] = from_date
                attrs["to_date"] = to_date
                future_add.append(attrs)
        self._append(future_add)


def generate_sliding_window_isoformat_intervals(from_date:datetime.date=None, to_date:datetime.date=None, window_shift_days:int=1, window_size_days:int=7) -> list:
    require_not_None(from_date)
    require_not_None(to_date)
    require_not_None(window_shift_days)
    require_not_None(window_size_days)

    # change range do default days
    to_date_mod = to_date - pd.Timedelta(window_size_days, 'd')
    if to_date_mod < from_date:
        raise Exception("invalid sliding windows configuration : from_date="+str(from_date)+" > "+str(to_date_mod)+" = to_date ("+str(to_date)+") - windows_size ("+str(window_size_days)+" days)")

    interval_starts = pd.date_range(start=from_date, end=to_date_mod, freq=str(window_shift_days) + "D")
    intervals = [(start_day.strftime('%Y-%m-%d'), (start_day + pd.Timedelta(window_size_days, 'd')).strftime('%Y-%m-%d')) for start_day in interval_starts]
    intervals = [(datetime.date.fromisoformat(require_not_None(itm[0])), datetime.date.fromisoformat(require_not_None(itm[1]))) for itm in intervals]
    return intervals

def generate_expanding_window_isoformat_intervals(from_date: datetime.date = None, to_date: datetime.date = None, window_increment_days: int=1) -> list:
    require_not_None(from_date)
    require_not_None(to_date)
    require_not_None(window_increment_days)

    # change range do default days
    to_date_mod = to_date - pd.Timedelta(window_increment_days, 'd')
    if to_date_mod < from_date:
        raise Exception("invalid expanding windows configuration : from_date=" + str(from_date) + " > " + str(
            to_date_mod) + " = to_date (" + str(to_date) + ") - windows_size (" + str(window_increment_days) + " days)")

    intervals = []
    end_tmp = from_date + pd.Timedelta(window_increment_days, 'd')
    while (end_tmp <= to_date_mod):
        intervals.append((datetime.date.fromisoformat(require_not_None(from_date.strftime('%Y-%m-%d'))), datetime.date.fromisoformat(require_not_None(end_tmp.strftime('%Y-%m-%d')))))
        end_tmp = end_tmp + pd.Timedelta(window_increment_days, 'd')

    return intervals

def generate_sliding_window_isoformat_intervals(from_date: datetime.date = None, to_date: datetime.date = None,
                                                window_shift_days: int = 1, window_size_days: int = 7) -> list:
    require_not_None(from_date)
    require_not_None(to_date)
    require_not_None(window_shift_days)
    require_not_None(window_size_days)

    # change range do default days
    to_date_mod = to_date - pd.Timedelta(window_size_days, 'd')
    if to_date_mod < from_date:
        raise Exception("invalid sliding windows configuration : from_date=" + str(from_date) + " > " + str(
            to_date_mod) + " = to_date (" + str(to_date) + ") - windows_size (" + str(window_size_days) + " days)")

    interval_starts = pd.date_range(start=from_date, end=to_date_mod, freq=str(window_shift_days) + "D")
    intervals = [
        (start_day.strftime('%Y-%m-%d'), (start_day + pd.Timedelta(window_size_days, 'd')).strftime('%Y-%m-%d')) for
        start_day in interval_starts]
    intervals = [
        (datetime.date.fromisoformat(require_not_None(itm[0])), datetime.date.fromisoformat(require_not_None(itm[1])))
        for itm in intervals]
    return intervals

def to_interval_str(from_date:datetime.date=None, to_date:datetime.date=None) -> str:
    return str(from_date).replace("-", "") + "_" + str(to_date).replace("-", "")

def remove_dir_if_exist(target:str=None):
    if os.path.exists(target):
        import shutil
        shutil.rmtree(target)

def remove_file_if_exist(target:str=None):
    if os.path.exists(target):
        os.remove(target)

def create_dir_if_not_exist(target:str=None):
    if not os.path.exists(target):
        try:
            os.mkdir(target)
        except OSError:
            print("Creation of the directory %s failed" % target)
        else:
            print("Successfully created the directory %s " % target)

def users_lookup(user_ids:list=None) -> List[tweepy.User]:
    import tweepy
    consumer_key = "hCHbwdLQK1An9YXq5DP20Kf5A"
    consumer_secret = "k9cfCKkiFuHJuW4UlnzSFHHgDamq3mRlcE72Q3egEm2a9pvvwV"
    access_token = "1155060925910700032-f1dTT1nw4EHNhbZ84xlObW59ByKu11"
    access_token_secret = "nenmWEU7AJXnmC9mvUig7Gai0BPgWPqclnjZUmahsh7JU"

    auth = tweepy.OAuth1UserHandler(
        consumer_key, consumer_secret, access_token, access_token_secret
    )

    api = tweepy.API(auth)

    res = []
    final_list = lambda test_list, x: [test_list[i:i + x] for i in range(0, len(test_list), x)]
    for chunk in final_list(user_ids, 100):
        res.extend(  api.lookup_users(user_id=chunk) )
    return res

