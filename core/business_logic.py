
from bicm import BipartiteGraph
import datetime

from igraph import VertexClustering, GraphBase

from scipy.sparse import coo_matrix

import core.utils as u
import pandas as pd
import numpy as np
import igraph as ig
import random
import os
import math

import urlexpander

from tqdm import tqdm


class URLEncoding(object):
    def __init__(self, encoding : dict = None):
        self._encoding = u.require_not_None( encoding )

    def get_value(self, key:int=None):
        u.require_not_None(key)
        value = self._encoding[key]
        return u.require_not_None(value)


def encode_key_in_dict(adjacency_list_tmp: dict = None):
    adjacency_list: dict = {}
    url_encoding: dict = {}
    curr_encoding: int = 0

    for item in list(adjacency_list_tmp.items()):
        url_encoding[curr_encoding] = item[0] #url_to_encode
        adjacency_list[curr_encoding] = item[1]
        curr_encoding += 1

    return adjacency_list, URLEncoding( url_encoding )


def produce_edge_list(adjacency_list_as_dict: dict = None) -> list:
    u.require_not_None(adjacency_list_as_dict)

    edge_list = []
    for k, v in list(adjacency_list_as_dict.items()):
        for itm in v:
            edge_list.append((itm, k))
    return edge_list


def LouR(g, reshuffles):
    #edgelist=[e.tuple for e in g.es]
    #nodelist=sorted(list(set([item for sublist in edgelist for item in sublist])))
    nodelist = list(range(len(g.vs)))
    Loug = g.community_multilevel(return_levels=False)
    edgelist = [g.es[i].tuple for i in range(len(g.es))]
    # Loug is a membership list, i.e. for every element the community it belongs to.
    mod = g.modularity(Loug)
    membership = Loug.membership

    for ii in range(reshuffles):
        #rifrullo=random.sample(list(range(len(nodelist))),len(nodelist))
        rifrullo=list(range(len(nodelist)))
        random.shuffle(rifrullo)
        newedges=[]
        for i in range(len(edgelist)):
            newedges.append((rifrullo[edgelist[i][0]],rifrullo[edgelist[i][1]]))
        gaux=ig.Graph()
        gaux.add_vertices(nodelist)
        gaux.add_edges(newedges)
        Lougaux=gaux.community_multilevel(return_levels=False)
        if mod<gaux.modularity(Lougaux):
            mod=gaux.modularity(Lougaux)
            membaux=Lougaux.membership
            for i in range(len(membaux)):
                membership[i]=membaux[rifrullo[i]]
            Loug=ig.VertexClustering(g,membership)
    return Loug


def check_attribute_presence_or_exeption(g:ig.Graph=None, attributes_to_check:list=None):
    u.require_not_None( g )
    u.require_not_None( attributes_to_check )

    # Check presence of member in graph
    for attr in attributes_to_check:
        all_attributes = g.vs.attributes()
        if attr not in all_attributes:
            raise Exception(attr + " : not in the list of graph's attributes")


def _execute_multiple_label_propagation(g:ig.Graph=None, num_runs:int=None):
    u.require_not_None( g )
    u.require_not_None( num_runs )

    _membership_matrix = np.zeros((num_runs, len(g.vs)), dtype='i4')

    for run_id in tqdm(range(num_runs), desc="Executing label propagation..."):

        # label propagation execution
        label_propagated:ig.VertexClustering = g.community_label_propagation(initial="ver_comm", fixed="fixed")

        # re-instance since for each run the label might be assigned in a different way
        mapping = LabelMappingBasedOnFixedVertexLabel( g, label_propagated )
        # label tranformation ( re-mapping into original )
        prop_label_mapped_into_original = mapping.compute_memba_after_label_propagation_and_mapping_into_original()

        # write the current label propagation results into the matrix
        for curr_vtx_id in range(len(g.vs)):
            _membership_matrix[run_id, curr_vtx_id] = int(prop_label_mapped_into_original[curr_vtx_id])

    # labels in which the majority of label propagation runs agree
    max_agreement_distribution = np.zeros(len(g.vs))
    for i in range(len(g.vs)):
        max_label_agreements = np.bincount(_membership_matrix[:, i]).argmax()
        max_agreement_distribution[i] = int(max_label_agreements)

    return max_agreement_distribution


class LabelPropagationExecuter(object):

    def __init__(self, g:ig.Graph=None, num_runs:int=1000):
        u.require_not_None(g)
        check_attribute_presence_or_exeption(g, ["ver_comm", "fixed"])
        u.require_not_None(num_runs)
        if num_runs < 1: raise Exception(str(num_runs) + " : invalid number of runs")

        self._g = g
        self._num_runs = num_runs
        self._executed = False

    def _set_executed(self):
        self._executed = True

    def get_result(self) -> ig.Graph:
        if not self._executed: raise Exception("label propagation not executed yet")
        return self._g

    def _get_graph(self):
        return u.require_not_None( self._g )

    def execute_label_propagation(self):
        self._get_graph().vs['ver_comm_prop'] = _execute_multiple_label_propagation( self._get_graph(), self._num_runs )

        # Final TAG assignment for not fixed vertex (exploiting label caming from fixed one)
        final_comm_TAG_assignment = {vtx["ver_comm_prop"]: vtx["ver_comm_TAG"] for vtx in self._get_graph().vs if vtx["fixed"]}
        def to_ver_comm_label(ver_comm):
            if ver_comm in final_comm_TAG_assignment:
                value = final_comm_TAG_assignment[ver_comm]
                if not isinstance(value, str): value = ""
                if value is not None and value != "":
                    return value
            return "UNC"

        self._get_graph().vs['ver_comm_label'] = [to_ver_comm_label(vtx["ver_comm_prop"]) for vtx in self._get_graph().vs]


        # POST CONDITION
        # after label propagation all fixed vertex labels has to be ver_comm_prop(final label) == ver_comm

        for vtx in self._get_graph().vs:
            if vtx["fixed"]:
                if vtx['ver_comm'] != vtx['ver_comm_prop']:
                    raise Exception(vtx["name"]+" : diff label init/after_label_propagation")

        self._set_executed()

class LabelMappingBasedOnFixedVertexLabel(object):

    def __init__(self, g:ig.Graph=None, memba_after_label_propagation:ig.VertexClustering=None):
        check_attribute_presence_or_exeption(g, ["name", "ver_comm", "fixed"])

        self._g:ig.Graph = u.require_not_None( g )
        self._memba_after_label_propagation = u.require_not_None( memba_after_label_propagation )

        self._mapping = {}

        for curr_vtx_id in range( len(g.vs) ):
            curr_vtx = g.vs[curr_vtx_id]

            if not curr_vtx[ "fixed" ]: continue

            original_label = curr_vtx[ "ver_comm" ]
            propagated_label = self._memba_after_label_propagation.membership[ curr_vtx_id ]

            if propagated_label in self._mapping:
                if self._mapping[propagated_label]!=original_label:
                    raise Exception(str(self._mapping[propagated_label])+" != "+str(original_label)+" : diff labels")
            else:
                self._mapping[propagated_label] = original_label


    #ritorna le label riassegnate tenendo presente le label originali dei vertex fixed
    def compute_memba_after_label_propagation_and_mapping_into_original(self) -> list:

        #return [self._mapping[propagated_label] for propagated_label in self._memba_after_label_propagation.membership]
        transposed_membership = []
        for propagated_label in self._memba_after_label_propagation.membership:
            transposed_membership.append( self._mapping[propagated_label] )

        return transposed_membership


class AbstractEdgeListProd(object):

    def __init__(self, df: pd.DataFrame = None, from_date:datetime.date = None, to_date:datetime.date = None, ignore_uniques_urls=False, filter_wout_url=True, lang:str=None):
        u.require_not_None(df)
        u.require_not_None(from_date)
        u.require_not_None(to_date)
        u.require_not_None(ignore_uniques_urls)

        # delete URLs that occur only one time (AIM. reduce network complexity)
        if ignore_uniques_urls:
            df = df[df.expanded_url.duplicated(keep=False)]

        if lang is not None:
            df = df.loc[df.lang==lang]

        #TODO perchè non eliminare anche user che compaiono una sola volta per ridurre la complessità della rete? CASO USR
        #eventually filter tweets without url
        if filter_wout_url:
            df = df[df.resolved_domain.notnull()]
        df = self._filter_by_date_interval(df, from_date, to_date)
        self._df: pd.DataFrame = df
        self._from_date:datetime.date = from_date
        self._to_date:datetime.date = to_date

    def _filter_by_date_interval(self, df: pd.DataFrame=None, from_date:datetime.date = None, to_date:datetime.date = None):
        u.require_not_None(df)
        _from_ = np.datetime64(u.require_not_None(from_date))
        _to_ = np.datetime64(u.require_not_None(to_date))

        df_tmp = df.loc[(df['created_at'] >= _from_)]
        df_tmp = df_tmp.loc[(df['created_at'] <= _to_)]
        return df_tmp

    def interval(self):
        return (self._from_date, self._to_date)

    def df(self) -> pd.DataFrame:
        return u.require_not_None(self._df)

    def pure_tweets(self) -> pd.DataFrame:
        return self.df().loc[(self.df().is_retweet == 'False') & (self.df().is_reply == 'False') & (self.df().is_quoted == 'False')]

    def pure_retweets(self) -> pd.DataFrame:
        return self.df().loc[(self.df().is_retweet == 'True') & (self.df().is_reply == 'False') & (self.df().is_quoted == 'False')]

    def pure_replies(self) -> pd.DataFrame:
        return self.df().loc[(self.df().is_retweet == 'False') & (self.df().is_reply == 'True') & (self.df().is_quoted == 'False')]

    def quoted_retweets(self) -> pd.DataFrame:
        return self.df().loc[(self.df().is_retweet == 'True') & (self.df().is_reply == 'False') & (self.df().is_quoted == 'True')]

    def quoted_tweets(self) -> pd.DataFrame:
        return self.df().loc[(self.df().is_retweet == 'False') & (self.df().is_reply == 'False') & (self.df().is_quoted == 'True')]

    def quoted_replies(self) -> pd.DataFrame:
        return self.df().loc[(self.df().is_retweet == 'False') & (self.df().is_reply == 'True') & (self.df().is_quoted == 'True')]

    def produce_adjacency_list_as_dict(self) -> dict:
        raise Exception("the method is not implemented yet!")

    def produce_edge_list(self) -> list:
        raise Exception("The method is not implemented yet!")

    def produce_biadjacency_matrix(self) -> np.array:
        raise Exception("The method is not implemented yet!")

    def tag(self):
        raise Exception("method not implemented yet!")

class UsersDomainWeightedEdgeList(AbstractEdgeListProd):

    def __init__(self, df: pd.DataFrame = None, from_date:datetime.date = None, to_date:datetime.date = None, ignore_uniques_urls:bool = False, lang:str=None):
        super().__init__(df, from_date, to_date, ignore_uniques_urls, lang=lang, filter_wout_url=True)

    def produce_edge_list(self) -> list:

        def _append_to_list_OLD(_edge:list=None, _to_append:pd.DataFrame=None):
            u.require_not_None(_edge)
            u.require_not_None(_to_append)

            from tqdm import tqdm
            for index, row in tqdm(_to_append.iterrows(), desc="Producing Edge List", total=len(_to_append.index)):
                _edge.append( (row["user_id"], row["resolved_domain"]) )
                #_edge.append((row["user_id"], row["expanded_url"]))
            return _edge

        edges = []
        edges = super()._append_to_list(edges, self.pure_tweets())
        edges = _append_to_list(edges, self.pure_retweets())
        edges = _append_to_list(edges, self.pure_replies())
        return edges

    def produce_edge_list(self) -> list:

        from tqdm import tqdm

        def _to_duplicated_edge_dataframe(_to_append:pd.DataFrame=None):
            u.require_not_None(_to_append)

            _user_ids = []
            _domains = []

            for index, row in tqdm(_to_append.iterrows(), desc="Producing Edge List", total=len(_to_append.index)):
                _user_ids.append( row["user_id"] )
                _domains.append( row["resolved_domain"] )

            return pd.DataFrame({"user_id" : _user_ids, "resolved_domain" : _domains})


        def _to_list(df):
            return [(int(row["user_id_enc"]), int(row["resolved_domain_enc"]), int(row["count"])) for index, row in df.iterrows()]

        edges = []
        edges.append( _to_duplicated_edge_dataframe( self.pure_tweets() ) )
        edges.append( _to_duplicated_edge_dataframe( self.pure_retweets() ))
        edges.append( _to_duplicated_edge_dataframe( self.pure_replies() ) )

        weighted_edge_list = pd.concat( edges ).groupby(['user_id', 'resolved_domain']).size().sort_values().reset_index(name="count")
        unique_user_ids = weighted_edge_list["user_id"].unique().tolist()
        unique_domains = weighted_edge_list["resolved_domain"].unique().tolist()

        enum_v_rows = list(enumerate(set(unique_user_ids)))
        enum_v_cols = list(enumerate(set(unique_domains)))

        from_enc_to_rows_v = {enc[0]: enc[1] for enc in enum_v_rows}
        from_enc_to_cols_v = {enc[0]: enc[1] for enc in enum_v_cols}

        from_rows_v_to_enc = {enc[1]: enc[0] for enc in enum_v_rows}
        from_cols_v_to_enc = {enc[1]: enc[0] for enc in enum_v_cols}

        weighted_edge_list["user_id_enc"] = weighted_edge_list.apply(lambda row: from_rows_v_to_enc[row["user_id"]], axis=1)
        weighted_edge_list["resolved_domain_enc"] = weighted_edge_list.apply(lambda row: from_cols_v_to_enc[row["resolved_domain"]], axis=1)

        return _to_list( weighted_edge_list ), from_enc_to_rows_v, from_enc_to_cols_v

    def produce_biadjacency_matrix(self) -> np.array:

        def _from_weighted_edge_list_to_coo_matrix(weighted_edge_list: list = None, num_rows : int = None, num_cols : int = None):
            u.require_not_None(weighted_edge_list)
            u.require_not_None(num_rows)
            u.require_not_None(num_cols)

            rows, cols, weights = zip(*[(row, col, weight+0.01) for row, col, weight in weighted_edge_list])
            sparse_coo_matrix = coo_matrix((weights, (rows, cols)), shape=(num_rows, num_cols))

            print(sparse_coo_matrix)

            return sparse_coo_matrix


        weighted_edge_list, from_enc_to_users_v, from_enc_to_domains_v = self.produce_edge_list()

        _coo_matrix = _from_weighted_edge_list_to_coo_matrix(weighted_edge_list, len(from_enc_to_users_v), len(from_enc_to_domains_v))
        import scipy as s
        s.sparse.save_npz("sparse_matrix.npz", _coo_matrix)
        return _coo_matrix, from_enc_to_users_v, from_enc_to_domains_v



    def produce_biadjacency_matrix_OLD(self) -> np.array:
        def _from_edge_list_to_weighted_biadiacency_matrix(edge_list: list = None):
            v_rows = [itm[1] for itm in edge_list]
            v_cols = [itm[0] for itm in edge_list]

            enum_v_rows = list(enumerate(set(v_rows)))
            enum_v_cols = list(enumerate(set(v_cols)))

            from_enc_to_rows_v = {enc[0]: enc[1] for enc in enum_v_rows}
            from_enc_to_cols_v = {enc[0]: enc[1] for enc in enum_v_cols}

            from_rows_v_to_enc = {enc[1]: enc[0] for enc in enum_v_rows}
            from_cols_v_to_enc = {enc[1]: enc[0] for enc in enum_v_cols}

            b_matrix = np.zeros((len(enum_v_rows), len(enum_v_cols)), dtype='float32')

            pd_edgelist = pd.DataFrame({'cols': v_cols, 'rows': v_rows})

            gb = pd_edgelist.groupby(['cols', 'rows']).size().sort_values().reset_index(name="count")
            print(gb)
            first = True
            for index, row in gb.iterrows():
                col_v, row_v, count_v = row['cols'], row['rows'], row['count']
                enc_col_v = from_cols_v_to_enc[col_v]  # giusta ecc se dato non trovato
                enc_row_v = from_rows_v_to_enc[row_v]  # giusta ecc se dato non trovato
                b_matrix[enc_row_v][enc_col_v] = count_v+0.001 if first else count_v

            return b_matrix, from_enc_to_rows_v, from_enc_to_cols_v



        edge_list = self.produce_edge_list()
        return _from_edge_list_to_weighted_biadiacency_matrix(edge_list)



class UserURLEdgeList(AbstractEdgeListProd):
    def __init__(self, df: pd.DataFrame = None, from_date:datetime.date = None, to_date:datetime.date = None, ignore_uniques_urls:bool = False, lang:str=None):
        super().__init__(df, from_date, to_date, ignore_uniques_urls, lang=lang)

    def _append_to_dict(self, d: dict, to_append: pd.DataFrame, analysis_type:str=None, item_builder=None) -> dict:
        ckeck_analysis_type(analysis_type)
        u.require_not_None(item_builder)

        from tqdm import tqdm
        for index, row in tqdm(to_append.iterrows(), desc="Producing Edge List", total=len(to_append.index)):

            # Rete bipartita short URL - utenti

            user_id: str = row['user_id']
            key: str = row['link_url_to_expand']

            if key is None: raise Exception("null value!")
            if not isinstance(key, str):
                print(row)
                raise Exception("not string!")

            if key not in d.keys():
                d[key] = []
            if user_id not in d[key]:
                d[key].append( item_builder( row ) )
        return d

    def produce_adjacency_list_as_dict(self, analysis_type=None) -> dict:
        ckeck_analysis_type(analysis_type)

        user_by_url = {}
        user_by_url = self._append_to_dict(user_by_url, self.pure_tweets(), analysis_type, item_builder= lambda row : row['user_id'])
        user_by_url = self._append_to_dict(user_by_url, self.pure_retweets(), analysis_type, item_builder= lambda row : row['user_id'])
        return user_by_url

    def produce_verbose_adjacency_list_as_dict(self, analysis_type=None, item_builder=None) -> dict:
        ckeck_analysis_type(analysis_type)
        u.require_not_None(item_builder)

        user_by_url = {}
        user_by_url = self._append_to_dict(user_by_url, self.pure_tweets(), analysis_type, item_builder=item_builder)
        user_by_url = self._append_to_dict(user_by_url, self.pure_retweets(), analysis_type, item_builder=item_builder)
        return user_by_url


class UserVerNotVerEdgeList(AbstractEdgeListProd):

    def __init__(self, df: pd.DataFrame = None, from_date:datetime.date = None, to_date:datetime.date = None):
        super().__init__(df, from_date, to_date, filter_wout_url=False)

    # Rete bipartita utenti VERIFICATI - NON VERIFICATI
    def _append_to_dict(self, d: dict, to_append: pd.DataFrame) -> dict:
        from tqdm import tqdm
        for index, row in tqdm(to_append.iterrows(), desc="Buiding Adj. list UNV/VER users", total=len(to_append.index)):

            _vu = None
            _uu = None

            xor_is_true = row['retweeted_verified'] != row['verified']

            if xor_is_true and row['retweeted_verified'] and not row['verified']:
                # in this case, the verified user is the retweeter
                _vu = row['retweeted_user_id']  # aux['retweeted_status']['user']['id_str']
                _uu = row['user_id']
            elif xor_is_true and not row['retweeted_verified'] and row['verified']:
                _uu = row['retweeted_user_id']  # aux['retweeted_status']['user']['id_str']
                _vu = row['user_id']
            elif not xor_is_true :
                continue
            else:
                raise Exception("case not handled")

            if _vu is None or _uu is None: raise Exception("null value")

            value: str = _uu
            key: str = _vu

            if key is None: raise Exception("null value!")
            if not isinstance(key, str):
                print(row)
                raise Exception("not string!")

            if key not in d.keys():
                d[key] = []
            if value not in d[key]:
                d[key].append(value)

        return d

    def produce_adjacency_list_as_dict(self) -> dict:
        unverified_by_verified = {}
        rt = self.pure_retweets()
        print("#verified_rt", str(len(rt.loc[rt["verified"] == True].index)))
        print("#unvverified_rt", str(len(rt.loc[rt["verified"] == False].index)))
        print("#verified_rt_users", str(rt.loc[rt["verified"] == True]["user_id"].nunique()))
        print("#unvverified_rt_users", str(rt.loc[rt["verified"] == False]["user_id"].nunique()))
        print(rt.nunique())
        unverified_by_verified = self._append_to_dict(unverified_by_verified, rt)
        return unverified_by_verified


    def tag(self):
        return "ver_unv"


class RtNetworkEdgeList(AbstractEdgeListProd):

    def __init__(self, df: pd.DataFrame = None, from_date:datetime.date = None, to_date:datetime.date = None, filter_wout_url=False):
        super().__init__(df, from_date, to_date, filter_wout_url=filter_wout_url)

    # Rete RT
    def _append_to_dict(self, d: dict, to_append: pd.DataFrame) -> dict:

        for index, row in tqdm(to_append.iterrows(), desc="Buiding Adj. list", total=len(to_append.index)):
            if row['is_retweet']=='False':raise Exception(str(type(row['is_retweet']))," : invalid type")

            value: str = row['retweeted_user_id']
            key: str = row['user_id']

            if key is None: raise Exception("null value!")
            if not isinstance(key, str):
                print(row)
                raise Exception("not string!")

            if key not in d.keys():
                d[key] = []
            if value not in d[key]:
                d[key].append(value)

        return d

    def produce_adjacency_list_as_dict(self) -> dict:
        adjacency_list = {}
        adjacency_list = self._append_to_dict(adjacency_list, self.pure_retweets())
        return adjacency_list

    def tag(self):
        return "rt_network"





def from_dict_to_uniques_node(d:dict=None) -> list:
    tmp = []
    for k, v in d.items():
        tmp.append(k)
        [tmp.append(i) for i in v]
    return list(np.unique(tmp))


def ckeck_analysis_type(type:str=None):
    u.require_not_None( type )
    if (type != "user/url") and (type!= "ver_unv"): raise Exception(type + " : type is not handled")



def execute_url_layer_analysis(
        adj_list_of_url_layer_projection:dict=None,
        encoding:URLEncoding=None,
        url_rating_map:dict=None):
    print("[execute_url_layer_analysis] cheking args...")

    u.require_not_None(adj_list_of_url_layer_projection)
    u.require_not_None(encoding)
    u.require_not_None(url_rating_map)

    print("[export] saving...")

    projection: ig.Graph = ig.Graph(directed=False)

    print("[igraph] adding vertexes...")
    [projection.add_vertex(
        name=encoding.get_value(vtx),
        longURL=url_rating_map[encoding.get_value(vtx)]["longURL"],
        domain=urlexpander.get_domain(encoding.get_value(vtx)),
        score=url_rating_map[encoding.get_value(vtx)]["score"],
        rating=url_rating_map[encoding.get_value(vtx)]["rating"])
        for vtx in from_dict_to_uniques_node(adj_list_of_url_layer_projection)]

    print("[igraph] adding edge...")
    [projection.add_edge(
        source=encoding.get_value(source),
        target=encoding.get_value(target))
        for source, target in produce_edge_list(adj_list_of_url_layer_projection)]

    #focus on largest connected components
    lcc = projection.clusters().giant()

    print("community detection...")
    vg_lou = LouR(lcc, 1000)
    lcc.vs['comm'] = vg_lou.membership

    #community detection results
    lcc.vs['comm_louvien'] = vg_lou.membership
    lcc.vs['comm_leiden'] = lcc.community_leiden(n_iterations=1000).membership
    lcc.vs['comm_infomap'] = lcc.community_infomap(trials=10).membership
    print("community detection DONE")

    return lcc


def execute_usr_layer_analysis(adj_list_of_usr_layer_projection:dict=None, usr_ver_map:dict=None):
    u.require_not_None(adj_list_of_usr_layer_projection)
    u.require_not_None(usr_ver_map)

    def map(usr_ver_map, vtx) -> bool:
        return usr_ver_map[vtx]["verified"]

    usr_lev_projection: ig.Graph = ig.Graph(directed=False)

    [usr_lev_projection.add_vertex(name=vtx,
                                   verified=map(usr_ver_map, vtx)
                                   ) for vtx in from_dict_to_uniques_node(adj_list_of_usr_layer_projection)]

    [usr_lev_projection.add_edge(source=source, target=target) for source, target in
     produce_edge_list(adj_list_of_usr_layer_projection)]

    # focus on largest connected components
    lcc = usr_lev_projection.clusters().giant()

    vg_lou = LouR(lcc, 1000)
    lcc.vs['comm'] = vg_lou.membership

    return lcc

def execute_verified_usr_layer_analysis(adj_list_of_usr_layer_projection:dict=None, usr_ver_map:dict=None):
    u.require_not_None(adj_list_of_usr_layer_projection)
    u.require_not_None(usr_ver_map)

    def map(usr_ver_map, vtx) -> bool:
        return usr_ver_map[vtx]["verified"]
    def map_screen_name(usr_ver_map, vtx) -> bool:
        return "" if "screen_name" not in usr_ver_map[vtx] else usr_ver_map[vtx]["screen_name"]
    def map_descr(usr_ver_map, vtx) -> bool:
        return "" if "description" not in usr_ver_map[vtx] else usr_ver_map[vtx]["description"]

    usr_lev_projection: ig.Graph = ig.Graph(directed=False)
    [usr_lev_projection.add_vertex(name=vtx,
                                   verified=map(usr_ver_map, vtx),
                                   screen_name=map_screen_name(usr_ver_map, vtx),
                                   description=map_descr(usr_ver_map, vtx)) for vtx in from_dict_to_uniques_node(adj_list_of_usr_layer_projection)]
    [usr_lev_projection.add_edge(source=source, target=target) for source, target in
     produce_edge_list(adj_list_of_usr_layer_projection)]

    # focus on largest connected components
    lcc = usr_lev_projection.clusters().giant()

    vg_lou = LouR(lcc, 1000)
    lcc.vs['comm'] = vg_lou.membership

    return lcc

def organize_map_usr_ver(df: pd.DataFrame = None):
    u.require_not_None(df)
    user_details_by_uid: dict = {}
    for index, row in df.iterrows():

        key = row['user_id']
        if key not in user_details_by_uid:
            user_details_by_uid[ key ] = {}
            user_details_by_uid[ key ]["verified"] = row['verified']

            if 'screen_name' in row:
                user_details_by_uid[ key ]["screen_name"] = row['screen_name']
            if 'user_description_if_verified' in row:
                user_details_by_uid[ key ]["description"] = row['user_description_if_verified']

        if row['is_retweet']:
            key2 = row['retweeted_user_id']
            if key2 not in user_details_by_uid:
                user_details_by_uid[ key2 ] = {}
                user_details_by_uid[ key2 ]["verified"] = row['retweeted_verified']

                if 'retweeted_screen_name' in row:
                    user_details_by_uid[ key2 ]["screen_name"] = row['retweeted_screen_name']
                if 'retweeted_user_description_if_verified' in row:
                    user_details_by_uid[ key2 ]["description"] = row['retweeted_user_description_if_verified']

    return user_details_by_uid



def organize_map_url_rating(df: pd.DataFrame = None, type: str = None) -> dict:
    u.require_not_None(df)
    ckeck_analysis_type(type)
    details_by_X: dict = {}
    for index, row in df.iterrows():
        key = row['link_url_to_expand']#shortURL
        details_by_X[key] = {"rating": row['Rating'], "score": row['Score'], "longURL" : row['expanded_url']}
    return details_by_X

def execute_projection(from_date:datetime.date=None, to_date:datetime.date=None, csv_in_path:str=None, ignore_uniques_urls=None):
    u.require_not_None(from_date)
    u.require_not_None(to_date)
    u.require_not_None(csv_in_path)
    u.require_not_None(ignore_uniques_urls)

    analysis_type = "user/url"  # TODO "url" or "domain"

    df: pd.DataFrame = pd.read_csv(csv_in_path, dtype=str)
    df['created_at'] = pd.to_datetime(df['created_at'], format='%a %b %d %H:%M:%S %z %Y').dt.tz_localize(None)

    edge_prod: UserURLEdgeList = UserURLEdgeList(df, from_date, to_date, ignore_uniques_urls=ignore_uniques_urls)
    adjacency_list: dict = edge_prod.produce_adjacency_list_as_dict(analysis_type)
    adjacency_list, encoding = encode_key_in_dict(adjacency_list)

    # Utility MAP shortURL: {'Rating', 'Score', 'longURL'}
    url_rating_map: dict = organize_map_url_rating( edge_prod.df(), analysis_type)
    usr_ver_map: dict = organize_map_usr_ver( edge_prod.df() )

    # Compute projection
    myGraph: BipartiteGraph = BipartiteGraph(adjacency_list=adjacency_list)
    my_probability_matrix = myGraph.get_bicm_matrix()
    my_x, my_y = myGraph.get_bicm_fitnesses()

    url_level_projection = execute_url_layer_analysis(adj_list_of_url_layer_projection=myGraph.get_rows_projection(),
                                            encoding=encoding,
                                            url_rating_map=url_rating_map)

    usr_level_projection = execute_usr_layer_analysis(
        adj_list_of_usr_layer_projection=myGraph.get_cols_projection(), usr_ver_map=usr_ver_map)

    myGraph.clean_edges()

    return url_level_projection, usr_level_projection

def execute_discussion_comm_analysis(csv_in_path: str = None,
                     interval = None,
                     experiment_output_dir: str = None):

    def _generate_prefix_for_img(interval=None) -> str:
        return u.to_interval_str(from_date=interval[0], to_date=interval[1])

    u.require_not_None(csv_in_path)
    u.require_not_None(interval)
    u.require_not_None(experiment_output_dir)

    df: pd.DataFrame = pd.read_csv(csv_in_path, dtype=str)
    print("Loaded: ", csv_in_path)
    df['created_at'] = pd.to_datetime(df['created_at'], format='%a %b %d %H:%M:%S %z %Y').dt.tz_localize(None)
    df['verified'] = df['verified'].map({'True': True, 'False': False})
    df['retweeted_verified'] = df['retweeted_verified'].map({'True': True, 'False': False})

    edge_prod: UserVerNotVerEdgeList = UserVerNotVerEdgeList(df, interval[0], interval[1])
    print("Producing adjacency list...")
    adjacency_list: dict = edge_prod.produce_adjacency_list_as_dict()
    print("Organize map user details...")
    usr_ver_map: dict = organize_map_usr_ver(edge_prod.df())

    # Compute projection
    myGraph: BipartiteGraph = BipartiteGraph(adjacency_list=adjacency_list)
    my_probability_matrix = myGraph.get_bicm_matrix()
    my_x, my_y = myGraph.get_bicm_fitnesses()

    print("------------")
    print(myGraph.n_cols)
    print(myGraph.n_rows)
    print(myGraph.n_edges)
    print("----------")

    verified_usr_level_projection = execute_verified_usr_layer_analysis(
        adj_list_of_usr_layer_projection=myGraph.get_rows_projection(),
        usr_ver_map=usr_ver_map)

    # Exports verified user
    print("[export] saving...")
    verified_usr_level_projection.save(
        os.path.join(experiment_output_dir, "graph_verified_usr_" + _generate_prefix_for_img(interval) + ".graphml"))
    print("[export] verified user graphml OK")

    myGraph.clean_edges()
    verified_usr_level_projection.clear()

class AbstractEdgeListProdInstancer(object):
    def __init__(self):
        pass

    def instance(self) -> AbstractEdgeListProd:
        raise Exception("abstract method")


def execute_label_propagation_analysis_RTNetwork(csv_in_path: str = None,
                                                 csv_ver_comm_tagged: str = None,
                                                 interval: object = None,
                                                 experiment_output_dir: str = None) -> object:

    u.require_not_None(csv_in_path)
    u.require_not_None(csv_ver_comm_tagged)
    u.require_not_None(interval)
    u.require_not_None(experiment_output_dir)

    class MyInstancer(AbstractEdgeListProdInstancer):
        def __init__(self):
            pass

        def instance(self) -> AbstractEdgeListProd:
            df: pd.DataFrame = pd.read_csv(csv_in_path, dtype=str)
            print("Loaded: ", csv_in_path)
            df['created_at'] = pd.to_datetime(df['created_at'], format='%a %b %d %H:%M:%S %z %Y').dt.tz_localize(None)
            df['verified'] = df['verified'].map({'True': True, 'False': False})
            df['retweeted_verified'] = df['retweeted_verified'].map({'True': True, 'False': False})
            df = df.astype({"user_id": str, "retweeted_user_id": str})

            edge_prod: RtNetworkEdgeList = RtNetworkEdgeList(df, interval[0], interval[1])
            return edge_prod

    return execute_label_propagation_analysis(
        instancer=MyInstancer(),
        csv_ver_comm_tagged=csv_ver_comm_tagged,
        experiment_output_dir=experiment_output_dir)


def execute_label_propagation_analysis_VerUnvNetwork(csv_in_path: str = None,
                                                 csv_ver_comm_tagged: str = None,
                                                 interval=None,
                                                 experiment_output_dir: str = None):
    u.require_not_None(csv_in_path)
    u.require_not_None(csv_ver_comm_tagged)
    u.require_not_None(interval)
    u.require_not_None(experiment_output_dir)

    class MyInstancer(AbstractEdgeListProdInstancer):
        def __init__(self):
            pass

        def instance(self) -> AbstractEdgeListProd:
            df: pd.DataFrame = pd.read_csv(csv_in_path, dtype=str)
            print("Loaded: ", csv_in_path)
            df['created_at'] = pd.to_datetime(df['created_at'], format='%a %b %d %H:%M:%S %z %Y').dt.tz_localize(None)
            df['verified'] = df['verified'].map({'True': True, 'False': False})
            df['retweeted_verified'] = df['retweeted_verified'].map({'True': True, 'False': False})
            df = df.astype({"user_id": str, "retweeted_user_id": str})

            edge_prod: UserVerNotVerEdgeList = UserVerNotVerEdgeList(df, interval[0], interval[1])
            return edge_prod

    return execute_label_propagation_analysis(
        instancer=MyInstancer(),
        csv_ver_comm_tagged=csv_ver_comm_tagged,
        experiment_output_dir=experiment_output_dir)


def execute_label_propagation_analysis(
        instancer: AbstractEdgeListProdInstancer = None,
        csv_ver_comm_tagged: str = None,
        experiment_output_dir: str = None):

    def _generate_prefix_for_img(interval=None) -> str:
        return u.to_interval_str(from_date=interval[0], to_date=interval[1])

    def to_dict_row(row):
        return {
            "ver_comm": row['ver_comm'],
            "name": row['name'],
            "usr_TAG": row['usr_TAG'],
            "ver_comm_TAG": row['ver_comm_TAG'],
        }

    u.require_not_None(instancer)
    u.require_not_None(csv_ver_comm_tagged)
    u.require_not_None(experiment_output_dir)

    ver_comm_tags = pd.read_csv(csv_ver_comm_tagged, dtype=str)
    ver_comm_tags = ver_comm_tags[["user_id", "ver_comm", "name", "usr_TAG", "ver_comm_TAG"]]
    ver_comm_tags = ver_comm_tags.astype({"user_id": str})

    user_details_by_uid: dict = {row['user_id']: to_dict_row(row) for index, row in ver_comm_tags.iterrows()}

    edge_prod: AbstractEdgeListProd = instancer.instance()
    adjacency_list: dict = edge_prod.produce_adjacency_list_as_dict()

    # INIT Graph
    network: ig.Graph = ig.Graph(directed=False)

    for vtx in tqdm(from_dict_to_uniques_node(adjacency_list), desc="Adding vertex"):
        network.add_vertex(name=vtx)

    # Adding edges
    edges = produce_edge_list(adjacency_list)
    network.add_edges(edges)

    # focus on largest connected components
    lcc: ig.Graph = network.clusters().giant()

    def to_ver_comm(vtx, user_details_by_uid) -> int:
        curr_ver_comm = to_dict_value_if_exist(user_details_by_uid, vtx, "ver_comm")
        ver_comm = -1 if curr_ver_comm == "" else int(curr_ver_comm)  # giusta exception se tipo sbagliato
        return ver_comm

    to_dict_value_if_exist = lambda d, _user_id, key: d.get(_user_id)[key] if _user_id in d else ""

    lcc.vs["cname"] = [to_dict_value_if_exist(user_details_by_uid, vtx["name"], "name") for vtx in lcc.vs]
    lcc.vs["usr_TAG"] = [to_dict_value_if_exist(user_details_by_uid, vtx["name"], "usr_TAG") for vtx in lcc.vs]
    lcc.vs["ver_comm"] = [to_ver_comm(vtx["name"] , user_details_by_uid) for vtx in lcc.vs]
    lcc.vs["ver_comm_TAG"] = [to_dict_value_if_exist(user_details_by_uid, vtx["name"], "ver_comm_TAG") for vtx in lcc.vs]
    lcc.vs["fixed"] = [False if curr_ver_comm_TAG == -1 else True for curr_ver_comm_TAG in lcc.vs["ver_comm"]]

    exec:LabelPropagationExecuter = LabelPropagationExecuter(lcc)
    exec.execute_label_propagation()
    labeled_lcc: ig.Graph = exec.get_result()

    # Exports verified user
    print("[export] saving...")
    print( labeled_lcc.summary() )
    out = os.path.join(experiment_output_dir, "label_prop_on_"+edge_prod.tag()+"_" + _generate_prefix_for_img(edge_prod.interval()) + ".graphml")
    labeled_lcc.save( out )
    print("[export] rt_network_ graphml OK : ",out)

    network.clear()


class UserInfoRegister(object):

    def __init__(self, args=None):
        self.indexed_register = u.require_not_None(args[0])
        self._attribute_names = u.require_not_None(args[1])

    def isUserPresent(self, user_id:str=None):
        return user_id in self.indexed_register

    def getAttributesByUser(self, user_id:str=None, attribute_name:str=None):
        u.require_not_None( user_id )
        u.require_not_None( attribute_name )

        if not self.isUserPresent( user_id ):
            raise Exception("{}: user id not found".format(str(user_id)))
        if attribute_name not in self.getAttributeNames():
            raise Exception(attribute_name + " : invalid attribute name")

        return self.indexed_register[user_id][attribute_name]

    def getAttributeNames(self) -> list:
        return self._attribute_names


class DatasetLoader(object):
    def __init__(self):
        pass

    def getEdgeListProd(self) -> AbstractEdgeListProd:
        raise Exception("getInstancer not implemented yet")

    def get_users_attribute_names(self) -> list:
        raise Exception("getDf not implemented yet")

    def get_user_attribute(self, user_id:str=None, attribute_name:str=None):
        raise Exception("getDf not implemented yet")

def produce_aggregated_by_usrnec_rt_network(
        loader: DatasetLoader = None,
        focus_on_giant:bool = True,
        experiment_output_dir: str = None,
        user_in_echo_chambers : dict = None
        ):
    def _generate_prefix_for_img(interval=None) -> str:
        return u.to_interval_str(from_date=interval[0], to_date=interval[1])

    def _compute_unique_nec(edges: pd.DataFrame = None) -> set:
        u_src = set(edges["v_comm_usr_source"].unique().tolist())
        u_dst = set(edges["v_comm_usr_destin"].unique().tolist())
        return u_src.union(u_dst)

    def _compute_number_user_per_nec(df):
        u_src = df[["v_comm_usr_source", "retweeted_user_id"]]
        u_dst = df[["v_comm_usr_destin", "user_id"]]

        u_src = u_src.rename(columns={
            "v_comm_usr_source": "v_comm_usr",
            "retweeted_user_id": "user_id"
        })
        u_dst = u_dst.rename(columns={
            "v_comm_usr_destin": "v_comm_usr",
            "user_id": "user_id"
        })

        res = pd.concat([u_src, u_dst])
        res = res.drop_duplicates(["v_comm_usr", "user_id"], keep="first")
        res: pd.DataFrame = res.groupby(["v_comm_usr"]).size().reset_index(name="size")
        d: dict = {}
        for index, row in res.iterrows():
            d[str(row["v_comm_usr"])] = int(row["size"])
        return d

    def extract_user_ids_into_lcc(edge_prod: AbstractEdgeListProd) -> dict:
        adjacency_list: dict = edge_prod.produce_adjacency_list_as_dict()

        # INIT Graph
        network: ig.Graph = ig.Graph(directed=True)

        for vtx in tqdm(from_dict_to_uniques_node(adjacency_list), desc="Adding vertex"):
            network.add_vertex(name=vtx)

        # Adding edges
        edges = produce_edge_list(adjacency_list)
        network.add_edges(edges)

        # cluster mode strong
        # focus on largest connected components if focus_on_giant==True
        lcc: ig.Graph = network.clusters(mode='weak').giant() if focus_on_giant else network
        print(str(len(lcc.vs)))
        lcc_users_by_id: dict = {vtx["name"]: "" for vtx in lcc.vs}
        print(str(len(lcc_users_by_id.keys())))

        network.clear()
        return lcc_users_by_id

    u.require_not_None(loader)
    u.require_not_None(experiment_output_dir)

    edge_prod: AbstractEdgeListProd = loader.getEdgeListProd()
    lcc_users_by_id = extract_user_ids_into_lcc( edge_prod )


    df = edge_prod.pure_retweets()
    # keep RT only if user_id is also contained into LCC
    df = df[df.apply(lambda x: (x['user_id'] in lcc_users_by_id) or (x['retweeted_user_id'] in lcc_users_by_id), axis=1)]

    if user_in_echo_chambers is not None:
        echo_chamber_users = {user_id: "" for user_id in user_in_echo_chambers}

        def if_belong_to_a_usrnec_is_also_inside_echo_chamber(row):
            if row['v_comm_usr_source'] == '-1' and row['v_comm_usr_destin'] == '-1': return True
            elif row['v_comm_usr_source'] != '-1' and row['v_comm_usr_destin'] == '-1': return row['retweeted_user_id'] in echo_chamber_users
            elif row['v_comm_usr_source'] == '-1' and row['v_comm_usr_destin'] != '-1': return row['user_id'] in echo_chamber_users
            elif row['v_comm_usr_source'] != '-1' and row['v_comm_usr_destin'] != '-1': return (row['retweeted_user_id'] in echo_chamber_users) and (row['user_id'] in echo_chamber_users)
            else: raise Exception("case not handled")

        #modifico source e destination in modo da considerare gli utenti che fanno parte delle user nec ma non delle echo chamber
        # come se fossero utenti esterni ( modo -1 ). In questo modo gli utenti che non fanno parte di -1 sono tutti parte di echo chambers
        def modify_values(row):
            if row['v_comm_usr_source'] == '-1' and row['v_comm_usr_destin'] == '-1':
                return row

            elif row['v_comm_usr_source'] != '-1' and row['v_comm_usr_destin'] == '-1':
                if row['retweeted_user_id'] not in echo_chamber_users:
                    row['v_comm_usr_source'] = '-1'

            elif row['v_comm_usr_source'] == '-1' and row['v_comm_usr_destin'] != '-1':
                if row['user_id'] not in echo_chamber_users:
                    row['v_comm_usr_destin'] = '-1'

            elif row['v_comm_usr_source'] != '-1' and row['v_comm_usr_destin'] != '-1':
                if (row['retweeted_user_id'] not in echo_chamber_users) and (row['user_id'] not in echo_chamber_users):
                    row['v_comm_usr_source'] = '-1'
                    row['v_comm_usr_destin'] = '-1'

                elif (row['retweeted_user_id'] not in echo_chamber_users) and (row['user_id'] in echo_chamber_users):
                    row['v_comm_usr_source'] = '-1'

                elif (row['retweeted_user_id'] in echo_chamber_users) and (row['user_id'] not in echo_chamber_users):
                    row['v_comm_usr_destin'] = '-1'

                elif (row['retweeted_user_id'] in echo_chamber_users) and (row['user_id'] in echo_chamber_users):
                    pass

            else:
                raise Exception("case not handled")
            return row

        # Applicare la funzione a tutte le righe del DataFrame
        df = df.apply(modify_values, axis=1)

        df = df[ df.apply(lambda x: if_belong_to_a_usrnec_is_also_inside_echo_chamber( x ), axis=1) ]

    edges: pd.DataFrame = df.groupby(['v_comm_usr_source', 'v_comm_usr_destin']).size().reset_index(name='size')

    # NOTA : elimino interazioni tra utenti al di fuori delle usr NEC
    #edges = edges.drop(edges[(edges["v_comm_usr_source"] == "-1") & (edges["v_comm_usr_destin"] == "-1")].index)

    unique_nec = _compute_unique_nec(edges)
    numb_user_per_nec = _compute_number_user_per_nec(df)

    g: ig.Graph = ig.Graph(directed=True)
    # Adding vertex
    for vtx in unique_nec:
        g.add_vertex(name=str(vtx), users=numb_user_per_nec[str(vtx)])
    # Adding edges
    for index, row in edges.iterrows():
        g.add_edge(str(row["v_comm_usr_source"]), str(row["v_comm_usr_destin"]), weight=int(row["size"]))

    lcc_str = "lcc_" if focus_on_giant else ""
    out = os.path.join(experiment_output_dir, _generate_prefix_for_img(
                           edge_prod.interval())+"_aggregated_rt_network_by_"+ lcc_str +"usrnec_type_with_self_on_out_only_echo_chamber_users_v1.graphml")
    g.save(out)
    print("[export] labeled_rt_network graphml OK : ", out)
    g.clear()


def produce_rt_network_with_community_type_labels(
        loader: DatasetLoader = None,
        focus_on_giant:bool = True,
        clustering_mode:str=None,
        experiment_output_dir: str = None):

    def _generate_prefix_for_img(interval=None) -> str:
        return u.to_interval_str(from_date=interval[0], to_date=interval[1])

    u.require_not_None(loader)
    u.require_not_None(experiment_output_dir)

    edge_prod: AbstractEdgeListProd = loader.getEdgeListProd()
    adjacency_list: dict = edge_prod.produce_adjacency_list_as_dict()

    # INIT Graph
    network: ig.Graph = ig.Graph(directed=True)

    for vtx in tqdm(from_dict_to_uniques_node(adjacency_list), desc="Adding vertex"):
        network.add_vertex(name=vtx)

    # Adding edges
    edges = produce_edge_list(adjacency_list)
    network.add_edges(edges)

    # focus on largest connected components if focus_on_giant==True

    # weakly
    lcc: ig.Graph = network.clusters(mode=clustering_mode).giant() if focus_on_giant else network

    for attr in loader.get_users_attribute_names():
        lcc.vs[ attr ] = [loader.get_user_attribute(vtx["name"], attr) for vtx in lcc.vs]

    # Exports verified user
    print("[export] saving...")
    print( lcc.summary() )
    out = os.path.join(experiment_output_dir, "rt_network_with_community_type_label_"+edge_prod.tag()+"_" + _generate_prefix_for_img(edge_prod.interval()) + "_lwcc.graphml")
    lcc.save( out )
    print("[export] labeled_rt_network graphml OK : ",out)

    network.clear()

def produce_rt_network_with_aggreagted_community_labels(
        loader: DatasetLoader = None,
        focus_on_giant:bool = True,
        experiment_output_dir: str = None):

    def _generate_prefix_for_img(interval=None) -> str:
        return u.to_interval_str(from_date=interval[0], to_date=interval[1])

    u.require_not_None(loader)
    u.require_not_None(experiment_output_dir)

    edge_prod: AbstractEdgeListProd = loader.getEdgeListProd()
    adjacency_list: dict = edge_prod.produce_adjacency_list_as_dict()

    # INIT Graph
    network: ig.Graph = ig.Graph(directed=True)

    for vtx in tqdm(from_dict_to_uniques_node(adjacency_list), desc="Adding vertex"):
        network.add_vertex(name=vtx)

    # Adding edges
    edges = produce_edge_list(adjacency_list)
    network.add_edges(edges)

    # focus on largest connected components if focus_on_giant==True

    # weakly
    lcc: ig.Graph = network.clusters().giant() if focus_on_giant else network

    for attr in loader.get_users_attribute_names():
        lcc.vs[ attr ] = [loader.get_user_attribute(vtx["name"], attr) for vtx in lcc.vs]

    # Exports verified user
    print("[export] saving...")
    print( lcc.summary() )
    out = os.path.join(experiment_output_dir, "rt_network_with_community_type_label_"+edge_prod.tag()+"_" + _generate_prefix_for_img(edge_prod.interval()) + ".graphml")
    lcc.save( out )
    print("[export] labeled_rt_network graphml OK : ",out)

    network.clear()

@DeprecationWarning
def execute_label_propagation_analysis_one_shot(
        instancer: AbstractEdgeListProdInstancer = None,
        csv_ver_comm_tagged: str = None,
        experiment_output_dir: str = None):

    def _generate_prefix_for_img(interval=None) -> str:
        return u.to_interval_str(from_date=interval[0], to_date=interval[1])

    def to_dict_row(row):
        return {
            "ver_comm": row['ver_comm'],
            "name": row['name'],
            "usr_TAG": row['usr_TAG'],
            "ver_comm_TAG": row['ver_comm_TAG'],
        }

    u.require_not_None(instancer)
    u.require_not_None(csv_ver_comm_tagged)
    u.require_not_None(experiment_output_dir)

    ver_comm_tags = pd.read_csv(csv_ver_comm_tagged, dtype=str)
    ver_comm_tags = ver_comm_tags[["user_id", "ver_comm", "name", "usr_TAG", "ver_comm_TAG"]]
    ver_comm_tags = ver_comm_tags.astype({"user_id": str})

    user_details_by_uid: dict = {row['user_id']: to_dict_row(row) for index, row in ver_comm_tags.iterrows()}

    edge_prod: AbstractEdgeListProd = instancer.instance()
    adjacency_list: dict = edge_prod.produce_adjacency_list_as_dict()

    # INIT Graph



    network: ig.Graph = ig.Graph(directed=False)

    for vtx in tqdm(from_dict_to_uniques_node(adjacency_list), desc="Adding vertex"):
        to_dict_value_if_exist = lambda d, _user_id, key: d.get(_user_id)[key] if _user_id in d else ""

        cname = to_dict_value_if_exist(user_details_by_uid, vtx, "name")
        usr_TAG = to_dict_value_if_exist(user_details_by_uid, vtx, "usr_TAG")
        curr_ver_comm = to_dict_value_if_exist(user_details_by_uid, vtx, "ver_comm")
        ver_comm = -1 if curr_ver_comm == "" else int(curr_ver_comm)  # giusta exception se tipo sbagliato
        ver_comm_TAG = to_dict_value_if_exist(user_details_by_uid, vtx, "ver_comm_TAG")
        fixed = False if ver_comm == -1 else True

        network.add_vertex(name=vtx, cname=cname, usr_TAG=usr_TAG, ver_comm=ver_comm, ver_comm_TAG=ver_comm_TAG,
                              fixed=fixed)

    # Adding edges
    edges = produce_edge_list(adjacency_list)
    network.add_edges(edges)

    # focus on largest connected components
    lcc: ig.Graph = network.clusters().giant()

    label_propagated: ig.VertexClustering = lcc.community_label_propagation(initial="ver_comm", fixed="fixed")
    lcc.vs['ver_comm_prop'] = label_propagated.membership

    def _decode(lcc:ig.Graph)->dict:
        import math
        r = {}
        for vtx in lcc.vs:
            key = int(vtx["ver_comm_prop"])
            value = vtx["ver_comm_TAG"]
            isnan = False
            if isinstance(value, float):
                isnan = math.isnan(value)
            if value is not None and value != "" and isnan==False and key not in r:
                r[key] = value
        return r

    ver_comm_decode: dict = _decode(lcc)

    # Rimane corrispondenza tra "label" e "ver_comm".
    # Aggiunto cosi il parametri perchè non ho capito come passare params a VertexClustering
    lcc.vs['ver_comm_label'] = [ver_comm_decode.get(ver_comm) for ver_comm in lcc.vs['ver_comm_prop']]

    # Exports verified user
    print("[export] saving...")
    print( lcc.summary() )
    out = os.path.join(experiment_output_dir, "label_prop_on_"+edge_prod.tag()+"_" + _generate_prefix_for_img(edge_prod.interval()) + ".graphml")
    lcc.save( out )
    print("[export] rt_network_ graphml OK : ",out)

    network.clear()



def execute_label_propagation_analysis_COPY(csv_in_path: str = None,
                     csv_ver_comm_tagged : str = None,
                     interval = None,
                     experiment_output_dir: str = None):

    def _generate_prefix_for_img(interval=None) -> str:
        return u.to_interval_str(from_date=interval[0], to_date=interval[1])

    def to_dict_row(row):
        return {
            "ver_comm": row['ver_comm'],
            "name": row['name'],
            "usr_TAG": row['usr_TAG'],
            "ver_comm_TAG": row['ver_comm_TAG'],
        }

    u.require_not_None(csv_in_path)
    u.require_not_None(csv_ver_comm_tagged)
    u.require_not_None(interval)
    u.require_not_None(experiment_output_dir)

    df: pd.DataFrame = pd.read_csv(csv_in_path, dtype=str)
    print("Loaded: ", csv_in_path)
    df['created_at'] = pd.to_datetime(df['created_at'], format='%a %b %d %H:%M:%S %z %Y').dt.tz_localize(None)
    df['verified'] = df['verified'].map({'True': True, 'False': False})
    df['retweeted_verified'] = df['retweeted_verified'].map({'True': True, 'False': False})
    df = df.astype({"user_id": str, "retweeted_user_id" : str})

    ver_comm_tags = pd.read_csv(csv_ver_comm_tagged, dtype=str)
    ver_comm_tags = ver_comm_tags[["user_id", "ver_comm", "name", "usr_TAG", "ver_comm_TAG"]]
    ver_comm_tags = ver_comm_tags.astype({"user_id": str})

    user_details_by_uid: dict = {row['user_id'] : to_dict_row(row) for index, row in ver_comm_tags.iterrows()}

    edge_prod: RtNetworkEdgeList = RtNetworkEdgeList(df, interval[0], interval[1])
    adjacency_list: dict = edge_prod.produce_adjacency_list_as_dict()

    #INIT Graph

    ver_comm_decode:dict = {int(row["ver_comm"]) : row["ver_comm_TAG"] for index, row in ver_comm_tags[["ver_comm", "ver_comm_TAG"]].drop_duplicates().iterrows()}

    rt_network: ig.Graph = ig.Graph(directed=False)

    for vtx in tqdm(from_dict_to_uniques_node(adjacency_list), desc="Adding vertex"):

        to_dict_value_if_exist = lambda d, _user_id, key: d.get(_user_id)[key] if _user_id in d else ""

        cname = to_dict_value_if_exist(user_details_by_uid, vtx, "name")
        usr_TAG = to_dict_value_if_exist(user_details_by_uid, vtx, "usr_TAG")
        curr_ver_comm = to_dict_value_if_exist(user_details_by_uid, vtx, "ver_comm")
        ver_comm = -1 if curr_ver_comm=="" else int(curr_ver_comm)#giusta exception se tipo sbagliato
        ver_comm_TAG = to_dict_value_if_exist(user_details_by_uid, vtx, "ver_comm_TAG")
        fixed = False if ver_comm==-1 else True

        rt_network.add_vertex(name=vtx, cname=cname, usr_TAG=usr_TAG, ver_comm=ver_comm, ver_comm_TAG=ver_comm_TAG, fixed=fixed)

    #Adding edges
    edges = produce_edge_list(adjacency_list)
    rt_network.add_edges(edges)

    # focus on largest connected components
    lcc: ig.Graph = rt_network.clusters().giant()

    label_propagated:ig.VertexClustering = lcc.community_label_propagation(initial="ver_comm", fixed="fixed")

    lcc.vs['ver_comm_prop'] = label_propagated.membership


    lcc.vs['ver_comm_prop_label'] = [ver_comm_decode.get( ver_comm_prop ) for ver_comm_prop in lcc.vs['ver_comm_prop']]

    # Exports verified user
    print("[export] saving...")
    lcc.save(os.path.join(experiment_output_dir, "rt_network_" + _generate_prefix_for_img(interval) + ".graphml"))
    print("[export] rt_network_ graphml OK")

    rt_network.clear()


def _private_save_degree_vs_ki_sqrt_L(path_out: str = None, inv_dict:dict=None, degree:np.ndarray=None, fitness:dict=None):
    u.require_not_None( path_out )
    u.require_not_None( inv_dict )
    u.require_not_None( degree )
    u.require_not_None( fitness )

    from dataclasses import make_dataclass
    import math

    Res = make_dataclass("Res", [("id", str), ("degree", int), ("fitness", float), ("L", int), ("degree_on_sqrt_of_L", int)])

    future_to_write = []

    L = degree.sum()
    for curr_node_name in inv_dict.keys():
        curr_node_idx = inv_dict[curr_node_name]
        curr_node_degree = degree[curr_node_idx]
        curr_node_fitness = fitness[curr_node_name]
        degree_on_sqrt_of_L = curr_node_degree / math.sqrt( L )

        future_to_write.append( Res( curr_node_name, curr_node_degree, curr_node_fitness, L, degree_on_sqrt_of_L ) )
        #print("name=", str(curr_node_name), " f=", str(curr_node_fitness), " degree=", str(curr_node_degree), " L=",str(L))

    to_export = pd.DataFrame( future_to_write )
    to_export.to_csv(path_out, index=False)
    print("Saved : ",path_out)



# Measure of K_i / sqrt(L) for each node in order to see if the model can be simplified
def save_degree_vs_ki_sqrt_L(path_out: str = None, myGraph: BipartiteGraph = None):
    u.require_not_None(path_out)
    u.require_not_None(myGraph)

    _private_save_degree_vs_ki_sqrt_L(
        os.path.join( path_out, "fitness_cols_layer.csv"),
        myGraph.inv_cols_dict,
        myGraph.cols_deg,
        myGraph.dict_y
    )
    _private_save_degree_vs_ki_sqrt_L(
        os.path.join(path_out, "fitness_rows_layer.csv"),
        myGraph.inv_rows_dict,
        myGraph.rows_deg,
        myGraph.dict_x
    )

def execute_weighted_domain_users_projection(
        csv_in_path: str = None,
        interval: list = None,
        experiment_output_dir: str = None,
        ignore_uniques_urls: bool = False,
        lang=None):

    u.require_not_None(csv_in_path)
    u.require_not_None(interval)
    u.require_not_None(experiment_output_dir)
    u.require_not_None(ignore_uniques_urls)

    def _generate_prefix_for_img(interval=None) -> str:
        return u.to_interval_str(from_date=interval[0], to_date=interval[1])

    from_date = interval[0]
    to_date = interval[1]

    analysis_type = "user/domain"  # TODO "url" or "domain"

    df: pd.DataFrame = pd.read_csv(csv_in_path, dtype=str)
    print("Loaded: ", csv_in_path)
    df['created_at'] = pd.to_datetime(df['created_at'], format='%a %b %d %H:%M:%S %z %Y').dt.tz_localize(None)
    df['verified'] = df['verified'].map({'True': True, 'False': False})
    df['retweeted_verified'] = df['retweeted_verified'].map({'True': True, 'False': False})

    edge_prod: UsersDomainWeightedEdgeList = UsersDomainWeightedEdgeList(df, from_date, to_date, ignore_uniques_urls=False, lang=lang)
    _biadjacency_matrix, encoding_of_rows_v, encoding_of_cols_v = edge_prod.produce_biadjacency_matrix()

    print("Rows : {}".format(len(encoding_of_rows_v)))
    print("Cols : {}".format(len(encoding_of_cols_v)))
    """# Utility MAP shortURL: {'Rating', 'Score', 'longURL'}
    url_rating_map: dict = organize_map_url_rating(edge_prod.df(), analysis_type)
    usr_ver_map: dict = organize_map_usr_ver(edge_prod.df())"""

    # Compute projection
    bg: BipartiteGraph = BipartiteGraph(biadjacency=_biadjacency_matrix)
    bg.set_to_continuous()
    bg.solve_tool(model='biwcm_c')
    _validated_matrix = bg.get_validated_matrix(significance=0.01, validation_method='fdr')

    #print("Rows Encoding: {}".format(from_enc_to_rows_v))
    #print("Cols Encoding: {}".format(from_enc_to_cols_v))
    print("Input Matrix (dtype={}): {}".format(_biadjacency_matrix.dtype, _biadjacency_matrix))
    print("Output Matrix (dtype={}): {}".format(_validated_matrix.dtype, _validated_matrix))
    """
    
    # my_probability_matrix = myGraph.get_bicm_matrix()
    dict_x, dict_y = myGraph.get_bicm_fitnesses()
    # myGraph.dict_x #key=nome_nodi, value=fitness_x

    # save_degree_vs_ki_sqrt_L(experiment_output_dir, myGraph)
    # exit(0)

    url_level_projection = execute_url_layer_analysis(
        adj_list_of_url_layer_projection=myGraph.get_rows_projection(),
        encoding=encoding,
        url_rating_map=url_rating_map)

    # Exports URLs NEC
    url_out = os.path.join(experiment_output_dir,
                           "projection_on_url_layer_" + _generate_prefix_for_img(interval) + ".graphml")
    url_level_projection.save(url_out)
    print("[export] URL graphml OK : ", url_out)

    # usr_level_projection = execute_usr_layer_analysis(adj_list_of_usr_layer_projection=myGraph.get_cols_projection(), usr_ver_map=usr_ver_map)

    # Exports USERs NEC
    """
    """usr_out = os.path.join(experiment_output_dir, "graph_usr_"+_generate_prefix_for_img(interval)+".graphml")
    usr_level_projection.save(usr_out)
    print("[export] usr graphml OK : ", usr_out)"""
    """
    myGraph.clean_edges()

    url_level_projection.clear()
    #usr_level_projection.clear()"""




#CREATA NUOVA FUNZIONE PER AVERE EXPORT RISULTATI INTERMEDI NON SOLO FINALI
#TODO QUESTA FUNZIONE DEVE ESSERE RISOSTITUITA CON execute_analysis_DECUPLED
def execute_analysis(csv_in_path: str = None,
                     intervals: list = None,
                     experiment_output_dir: str = None,
                     ignore_uniques_urls: bool = None,
                     lang=None):

    u.require_not_None(csv_in_path)
    u.require_not_None(intervals)
    u.require_not_None(experiment_output_dir)
    u.require_not_None(ignore_uniques_urls)


    def _generate_prefix_for_img(interval=None) -> str:
        return u.to_interval_str(from_date=interval[0], to_date=interval[1])

    #url_graph_exporter: u.GraphExporterManager = u.GraphExporterManager(output_file=os.path.join(experiment_output_dir, "url_community_data.csv"), sep=",")
    #user_graph_exporter: u.GraphExporterManager = u.GraphExporterManager(output_file=os.path.join(experiment_output_dir, "users_community_data.csv"), sep=",")

    url_plot: u.PlotAdapter = u.PlotAdapter(experiment_output_dir)
    user_plot: u.PlotAdapter = u.PlotAdapter(experiment_output_dir)

    for interval in intervals:
        from_date = interval[0]
        to_date = interval[1]

        analysis_type = "user/url"  # TODO "url" or "domain"

        df: pd.DataFrame = pd.read_csv(csv_in_path, dtype=str)


        print("Loaded: ",csv_in_path)
        df['created_at'] = pd.to_datetime(df['created_at'], format='%a %b %d %H:%M:%S %z %Y').dt.tz_localize(None)
        df['verified'] = df['verified'].map({'True': True, 'False': False})
        df['retweeted_verified'] = df['retweeted_verified'].map({'True': True, 'False': False})

        edge_prod: UserURLEdgeList = UserURLEdgeList(df, from_date, to_date, ignore_uniques_urls=ignore_uniques_urls, lang=lang)
        adjacency_list: dict = edge_prod.produce_adjacency_list_as_dict(analysis_type)
        adjacency_list, encoding = encode_key_in_dict(adjacency_list)

        # Utility MAP shortURL: {'Rating', 'Score', 'longURL'}
        url_rating_map: dict = organize_map_url_rating(edge_prod.df(), analysis_type)
        usr_ver_map: dict = organize_map_usr_ver(edge_prod.df())

        # Compute projection
        myGraph: BipartiteGraph = BipartiteGraph(adjacency_list=adjacency_list)
        #my_probability_matrix = myGraph.get_bicm_matrix()
        dict_x, dict_y = myGraph.get_bicm_fitnesses()
        #myGraph.dict_x #key=nome_nodi, value=fitness_x

        #save_degree_vs_ki_sqrt_L(experiment_output_dir, myGraph)
        #exit(0)


        url_level_projection = execute_url_layer_analysis(
            adj_list_of_url_layer_projection=myGraph.get_rows_projection(),
            encoding=encoding,
            url_rating_map=url_rating_map)

        #Exports URLs NEC
        url_out = os.path.join(experiment_output_dir, "projection_on_url_layer_"+_generate_prefix_for_img(interval)+".graphml")
        url_level_projection.save( url_out )
        print("[export] URL graphml OK : ",url_out)

        #usr_level_projection = execute_usr_layer_analysis(adj_list_of_usr_layer_projection=myGraph.get_cols_projection(), usr_ver_map=usr_ver_map)

        #Exports USERs NEC
        """
        usr_out = os.path.join(experiment_output_dir, "graph_usr_"+_generate_prefix_for_img(interval)+".graphml")
        usr_level_projection.save(usr_out)
        print("[export] usr graphml OK : ", usr_out)
        """
        myGraph.clean_edges()

        url_level_projection.clear()
        #usr_level_projection.clear()

    #u.remove_dir_if_exist(url_graph_exporter.output_file())
    #url_graph_exporter.flush()

    #u.remove_dir_if_exist(user_graph_exporter.output_file())
    #user_graph_exporter.flush()

#CREATA NUOVA FUNZIONE PER AVERE EXPORT RISULTATI INTERMEDI NON SOLO FINALI
#TODO DA UTILIZZARE AL POSTO DI execute_analysis

def execute_analysis_DECUPLED(csv_in_path: str = None,
                     intervals: list = None,
                     experiment_output_dir: str = None,
                     ignore_uniques_urls: bool = None):

    u.require_not_None(csv_in_path)
    u.require_not_None(intervals)
    u.require_not_None(experiment_output_dir)
    u.require_not_None(ignore_uniques_urls)

    def _generate_prefix_for_img(interval=None) -> str:
        return u.to_interval_str(from_date=interval[0], to_date=interval[1])

    u.remove_dir_if_exist(experiment_output_dir)
    u.create_dir_if_not_exist(experiment_output_dir)

    url_graph_exporter: u.GraphExporterManager = u.GraphExporterManager(output_file=os.path.join(experiment_output_dir, "url_community_data.csv"), sep=",")
    user_graph_exporter: u.GraphExporterManager = u.GraphExporterManager(output_file=os.path.join(experiment_output_dir, "users_community_data.csv"), sep=",")

    url_plot: u.PlotAdapter = u.PlotAdapter(experiment_output_dir)
    user_plot: u.PlotAdapter = u.PlotAdapter(experiment_output_dir)

    for interval in intervals:
        url_level_projection, usr_level_projection = execute_projection(from_date=interval[0], to_date=interval[1],
                                                                      csv_in_path=csv_in_path, ignore_uniques_urls=ignore_uniques_urls)

        # Export Graph ML

        url_level_projection.write_graphml(os.path.join(experiment_output_dir, "graph_url_"+_generate_prefix_for_img(interval)+".graphml"), format="graphml")
        usr_level_projection.write_graphml(os.path.join(experiment_output_dir, "graph_usr_"+_generate_prefix_for_img(interval)+".graphml"), format="graphml")

        # Export CSV

        url_graph_exporter.append_graph_export(u.require_not_None(url_level_projection), from_date=interval[0], to_date=interval[1])
        user_graph_exporter.append_graph_export(u.require_not_None(usr_level_projection), from_date=interval[0], to_date=interval[1])

        # Export IMG
        url_plot.plot_graph(img_name="url_"+_generate_prefix_for_img(interval) + "_comminities.pdf",
                            g=url_level_projection,
                            comm_color=True,
                            plot_name_lambda=lambda v_list: [v["rating"] if v["rating"] != "UNC" else "" for v in
                                                             v_list])
        user_plot.plot_graph(img_name="usr_"+_generate_prefix_for_img(interval) + "_comminities.pdf",
                             g=usr_level_projection,
                             comm_color=True,
                             plot_name_lambda=None)

        url_level_projection.clear()
        usr_level_projection.clear()

    u.remove_dir_if_exist(url_graph_exporter.output_file())
    url_graph_exporter.flush()

    u.remove_dir_if_exist(user_graph_exporter.output_file())
    user_graph_exporter.flush()

class SWExperimentalSettings(object):

    def __init__(self,
                 csv_in_path: str = None,
                 out_path: str = None,
                 from_date: datetime.date = None,
                 to_date: datetime.date = None,
                 window_shift_days: int = 1,
                 window_size_days: int = 7,
                 ignore_unique_urls=False,
                 csv_in_path_total=None, #FIXME brutto da vedere (TODO aggiungere filtro e compattare con csv_in_path)
                 total_from_date: datetime.date = None,
                 total_to_date : datetime.date = None,
                 csv_ver_comm_tagged: str = None
    ):

        self.csv_in_path = u.require_not_None(csv_in_path)
        self.out_path = u.require_not_None(out_path)
        self.from_date = u.require_not_None(from_date)
        self.to_date = u.require_not_None(to_date)
        self.total_from_date = u.require_not_None(total_from_date)
        self.total_to_date = u.require_not_None(total_to_date)

        self.window_shift_days = u.require_not_None(window_shift_days)
        self.window_size_days = u.require_not_None(window_size_days)
        csv_prefix = self._generate_prefix_for_csv(
            from_date, to_date,
            window_size_days=window_size_days,
            window_shift_days=window_shift_days
        )
        self.experiment_dir = os.path.join(out_path, csv_prefix)
        self.ignore_unique_urls = u.require_not_None( ignore_unique_urls )

        #Path al dataset completo (csv_in_path è il dataset AGGIUSTATO dalle inconzistanze LONG-SHORT URLs)
        self.csv_in_path_total = u.require_not_None( csv_in_path_total )

        self.csv_ver_comm_tagged = u.require_not_None( csv_ver_comm_tagged )


    def _generate_prefix_for_csv(self, from_date: datetime.date = None, to_date: datetime.date = None, window_size_days=None,
                                 window_shift_days=None) -> str:
        return "sliding_window_" + str(from_date).replace("-", "") + "_" + str(to_date).replace("-", "") + "_" + str(
            window_size_days) + "_size_" + str(window_shift_days) + "_shift"


def util_execute_sliding_window_analysis(settings: SWExperimentalSettings = None):
    u.require_not_None(settings)

    u.remove_dir_if_exist(settings.experiment_dir)
    u.create_dir_if_not_exist(settings.experiment_dir)

    intervals = u.generate_sliding_window_isoformat_intervals(settings.from_date, settings.to_date, settings.window_shift_days, settings.window_size_days)
    total_interval = (settings.total_from_date, settings.total_to_date)


    execute_analysis(
        settings.csv_in_path,
        intervals,
        settings.experiment_dir,
        settings.ignore_unique_urls
    )
    """
    execute_discussion_comm_analysis(
        settings.csv_in_path_total,
        total_interval,
        settings.experiment_dir,
    )
    
    execute_label_propagation_analysis_RTNetwork(
        settings.csv_in_path_total,
        settings.csv_ver_comm_tagged,
        total_interval,
        settings.experiment_dir,
    )"""



def util_execute_expanding_window_analysis(
        csv_in_path: str = None,
        out_path: str = None,
        from_date: datetime.date = None,
        to_date: datetime.date = None,
        window_increment_days: int=1,
        ignore_uniques_urls=None):

    u.require_not_None(csv_in_path)
    u.require_not_None(out_path)
    u.require_not_None(from_date)
    u.require_not_None(to_date)
    u.require_not_None(window_increment_days)
    u.require_not_None(ignore_uniques_urls)

    intervals = u.generate_expanding_window_isoformat_intervals(from_date, to_date, window_increment_days)

    def _generate_prefix_for_csv(from_date: datetime.date = None, to_date: datetime.date = None,
                                 window_increment_days=None) -> str:
        return "expanding_window_" + str(from_date).replace("-", "") + "_" + str(to_date).replace("-", "") + "_" + str(
            window_increment_days) + "_inc"

    csv_prefix = _generate_prefix_for_csv(from_date, to_date, window_increment_days=window_increment_days)
    experiment_dir = os.path.join(out_path, csv_prefix)

    execute_analysis(
        csv_in_path,
        intervals,
        experiment_dir,
        ignore_uniques_urls
    )