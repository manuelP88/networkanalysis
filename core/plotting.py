import warnings

import core.utils as u
import igraph as ig
import numpy as np

def communities_present(g:ig.Graph=None) -> bool:
    try: g.vs['comm']
    except KeyError:
        return False
    return True

def plot_graph(target:str=None, g:ig.Graph=None, comm_color:bool=True, plot_name_lambda=None):
    u.require_not_None(target)
    u.require_not_None(g)
    u.require_not_None(comm_color)

    u.remove_dir_if_exist(target)

    are_comm_present = communities_present(g)
    if comm_color and not are_comm_present:
        warnings.warn('missing g.vs[\'comm\'] arg: impossible to color communities')

    comm_color = comm_color and are_comm_present

    color_dict = {}
    if comm_color:
        # assign colors to the various communities
        igraph_colors = list(ig.known_colors.keys())
        igraph_colors = [ic for ic in igraph_colors if
                         ic.startswith('gray') == False and ic.startswith('grey') == False]
        # colors are chosen randomly
        true_colors = np.random.choice(igraph_colors, size=len(np.unique(g.vs['comm'])))
        for i in range(len(true_colors)):
            color_dict[i] = true_colors[i]

    visual_style = {}
    visual_style["layout"] = g.layout('fr')
    visual_style["label_size"]= 0.25
    visual_style["vertex_size"] = 1 + np.log(np.array(g.degree()))
    visual_style["edge_width"] = .1
    visual_style["edge_curved"] = .05
    visual_style["edge_color"] = 'gray6'

    #conditional configurations
    if plot_name_lambda is not None: visual_style["vertex_label"] = plot_name_lambda(g.vs)
    if comm_color: visual_style["vertex_color"] = [color_dict[i] for i in g.vs['comm']]

    ig.plot(g, **visual_style, target=target)
