import pandas as pd
import requests


def load_manipulate_and_marge_users_info(
        initial_dataset:pd.DataFrame=None,
        usr_graph_nodes_path:str=None,
        url_graph_nodes_path: str = None,
        discoursive_comm_nodes_path: str = None,
        attributes_name:list = None
):

    print("usrnec_node_path.{}".format(usr_graph_nodes_path))
    print("urlnec_node_path.{}".format(url_graph_nodes_path))
    print("dc_node_path.{}".format(discoursive_comm_nodes_path))

    usr_nodes: pd.DataFrame = pd.read_csv(usr_graph_nodes_path, dtype=str) # USR NEC
    urls_nodes: pd.DataFrame = pd.read_csv(url_graph_nodes_path, dtype=str) # URL NEC

    discoursive_comm_nodes: pd.DataFrame = pd.read_csv(discoursive_comm_nodes_path, dtype=str)
    discoursive_comm_nodes = discoursive_comm_nodes[
        ['v_name', 'v_ver_comm', 'v_ver_comm_prop', 'v_ver_comm_label', 'Degree_label_prop']]

    def correct_label_name(row):
        if row["v_ver_comm_label"] == "FI-L-Media":
            return "FdI-L-Media"
        elif row["v_ver_comm_label"] == "ItaliaViva-SocDem-Media":
            return "ItaV-PD-Media"
        else:
            return row["v_ver_comm_label"]

    discoursive_comm_nodes["v_ver_comm_label"] = discoursive_comm_nodes.apply(
        lambda row: correct_label_name(row), axis=1)

    # Manipulation

    urls_nodes_tmp = urls_nodes.rename(columns={
        "v_comm": "v_comm_urls",
        "v_name": "short_url"
    })
    usr_nodes_tmp = usr_nodes.rename(columns={
        "v_comm": "v_comm_usr",
        "v_name": "user_id"
    })

    urls_nodes_tmp = urls_nodes_tmp[['short_url', 'v_comm_urls', 'Orientation']]
    usr_nodes_tmp = usr_nodes_tmp[['user_id', 'v_comm_usr']]

    view = pd.merge(
        initial_dataset,
        urls_nodes_tmp,
        how="left",
        left_on=["link_url_to_expand"],
        right_on=["short_url"],
        left_index=False,
        right_index=False,
        sort=True,
        copy=True,
        indicator=False,
        validate="many_to_one"
    )

    view = pd.merge(
        view,
        usr_nodes_tmp,
        how="left",
        left_on=["user_id"],
        right_on=["user_id"],
        left_index=False,
        right_index=False,
        sort=True,
        copy=True,
        indicator=False,
        validate="many_to_one"
    )

    view = pd.merge(
        view,
        discoursive_comm_nodes,
        how="left",
        left_on=["user_id"],
        right_on=["v_name"],
        left_index=False,
        right_index=False,
        sort=True,
        copy=True,
        indicator=False,
        validate="many_to_one"
    )

    # v_comm_usr_destin, v_comm_usr_source
    if "v_comm_usr_source" in attributes_name and "v_comm_usr_destin" in attributes_name:

        view["v_comm_usr_destin"] = view.apply(lambda row: row["v_comm_usr"], axis=1)

        usr_nodes_tmp = usr_nodes.rename(columns={
            "v_comm": "v_comm_usr_source",
            "v_name": "retweeted_user_id"
        })
        view = pd.merge(
            view,
            usr_nodes_tmp,
            how="left",
            left_on=["retweeted_user_id"],
            right_on=["retweeted_user_id"],
            left_index=False,
            right_index=False,
            sort=True,
            copy=True,
            indicator=False,
            validate="many_to_one"
        )
        view['v_comm_usr_source'] = view['v_comm_usr_source'].fillna("-1")
        view['v_comm_usr_destin'] = view['v_comm_usr_destin'].fillna("-1")

    # NAMING

    # commutity USERs : 'v_comm_usr'
    # community URLs : 'v_comm_urls'
    # community Disco : 'v_ver_comm_label'

    view['v_comm_urls'] = view['v_comm_urls'].fillna("-1")
    view['v_comm_usr'] = view['v_comm_usr'].fillna("-1")
    view['v_ver_comm_label'] = view['v_ver_comm_label'].fillna("-1")

    view["disc_comm_flag"] = view.apply(lambda row: "False" if (row["v_ver_comm_label"] == "-1") else "True", axis=1)
    view["url_comm_flag"] = view.apply(lambda row: "False" if (row["v_comm_urls"] == "-1") else "True", axis=1)
    view["usr_comm_flag"] = view.apply(lambda row: "False" if (row["v_comm_usr"] == "-1") else "True", axis=1)

    view = view[attributes_name]

    return view

def build_custom_user_register(users_info:pd.DataFrame=None, attribute_names:list=None, classify_only_user_w_URL:bool=True):

    def process_user_for_analysis1(curr_usr_data:pd.DataFrame=None):
        # Ip. abbiamo solo Fdi
        dc_name = curr_user_data.iloc[0]["v_ver_comm_label"]
        if dc_name != "FdI-L-Media": raise Exception("{} is not handled".format(str(dc_name)))

        belongs_to_fdi:bool = len(curr_usr_data.loc[ (users_info["disc_comm_flag"]=="True") ].index) > 0
        belongs_to_a_usrnec:bool = len(curr_usr_data.loc[ (users_info["usr_comm_flag"]=="True") ].index) > 0
        belongs_to_at_least_one_urlnec:bool = len(curr_usr_data.loc[ (users_info["url_comm_flag"]=="True") ].index) > 0

        if belongs_to_fdi == True and belongs_to_a_usrnec == True and belongs_to_at_least_one_urlnec == True:
            #curr_usr_data.to_csv("C:\\Users\\manue\\Downloads\\out.csv")
            return "FdI & usrNEC & urlNEC"
        elif belongs_to_fdi == True and belongs_to_a_usrnec == False and belongs_to_at_least_one_urlnec == False:
            return "FdI & no-usrNEC & no-urlNEC"
        elif belongs_to_fdi == True and belongs_to_a_usrnec == False and belongs_to_at_least_one_urlnec == True:
            return "FdI & no-usrNEC & urlNEC"
        elif belongs_to_fdi == True and belongs_to_a_usrnec == True and belongs_to_at_least_one_urlnec == False:
            return "FdI & usrNEC & no-urlNEC"
        elif belongs_to_fdi == False and belongs_to_a_usrnec == False and belongs_to_at_least_one_urlnec == False:
            return "no-FdI & no-usrNEC & no-urlNEC"
        else:
            raise Exception("case not handled")

    def get_user_nec_community_types(curr_usr_data:pd.DataFrame=None):
        # Ip. abbiamo solo Fdi
        dc_name = curr_user_data.iloc[0]["v_ver_comm_label"]
        if dc_name != "FdI-L-Media": raise Exception("{} is not handled".format(str(dc_name)))

        belongs_to_a_usrnec:bool = len(curr_usr_data.loc[ (users_info["usr_comm_flag"]=="True") ].index) > 0
        belongs_to_at_least_one_urlnec:bool = len(curr_usr_data.loc[ (users_info["url_comm_flag"]=="True") ].index) > 0

        if belongs_to_a_usrnec == True and belongs_to_at_least_one_urlnec == True:
            return "usrNEC & urlNEC"
        elif belongs_to_a_usrnec == False and belongs_to_at_least_one_urlnec == False:
            return "-1"
        elif belongs_to_a_usrnec == False and belongs_to_at_least_one_urlnec == True:
            return "urlNEC"
        elif belongs_to_a_usrnec == True and belongs_to_at_least_one_urlnec == False:
            return "usrNEC"
        else:
            raise Exception("case not handled")

    def belongs_to_usrNEC(curr_usr_data:pd.DataFrame=None):
        # Ip. abbiamo solo Fdi
        dc_name = curr_user_data.iloc[0]["v_ver_comm_label"]
        if dc_name != "FdI-L-Media": raise Exception("{} is not handled".format(str(dc_name)))
        return len(curr_usr_data.loc[ (users_info["usr_comm_flag"]=="True") ].index) > 0

    def belongs_to_urlNEC(curr_usr_data:pd.DataFrame=None):
        # Ip. abbiamo solo Fdi
        dc_name = curr_user_data.iloc[0]["v_ver_comm_label"]
        if dc_name != "FdI-L-Media": raise Exception("{} is not handled".format(str(dc_name)))
        return len(curr_usr_data.loc[ (users_info["url_comm_flag"]=="True") ].index) > 0

    def numb_url_that_shares(curr_usr_data: pd.DataFrame = None) -> int:
        # Ip. abbiamo solo Fdi
        dc_name = curr_user_data.iloc[0]["v_ver_comm_label"]
        if dc_name != "FdI-L-Media": raise Exception("{} is not handled".format(str(dc_name)))
        return len( curr_user_data.loc[ (curr_user_data["link_url_to_expand"].notnull() ) ].index )


    def prob_to_share_url_with_rating(curr_user_data:pd.DataFrame=None, thr:int=None, rating_value:str=None):
        if thr!=0:raise Exception("Case not handled yet")#nel caso rivedere implementazione per adesso il caso non si dovrebbe verificare (vedi chi chiama la funzione)
        all_shared_url = curr_user_data.loc[ (curr_user_data["link_url_to_expand"].notnull() ) ]
        size_all_shared_url = len( all_shared_url.index )
        if size_all_shared_url==0: raise Exception("prob_to_share_url_with_rating cannot be estimated")
        if size_all_shared_url < thr or size_all_shared_url==0:
            return "_"
        return round(len( all_shared_url.loc[ all_shared_url[ "Rating" ] == rating_value ] ) / size_all_shared_url,2)

    def prob_to_share_link_with_rating(curr_user_data:pd.DataFrame=None, rating_value:str=None):
        all_shared_url = curr_user_data.loc[(curr_user_data["link_url_to_expand"].notnull())]
        tot = len(all_shared_url.index)
        return 0 if tot == 0 else round(len(all_shared_url.loc[all_shared_url["Rating"] == rating_value]) / tot, 2)

    def prob_range_to_share_url_with_rating(curr_user_data:pd.DataFrame=None, rating_value:str=None):
            prob = prob_to_share_link_with_rating(curr_user_data, rating_value)

            if prob >=0 and prob <0.33:
                return "l"
            elif prob >=0.33 and prob <0.66:
                return "m"
            else:
                return "h"

    def estimate_info_diet(curr_user_data: pd.DataFrame = None):
        all_shared_url = curr_user_data.loc[(curr_user_data["link_url_to_expand"].notnull())]
        if len( all_shared_url.index )==0:
            raise Exception("info diet cannot be estimated")

        prob_T = prob_to_share_link_with_rating(curr_user_data, "T")
        prob_N = prob_to_share_link_with_rating(curr_user_data, "N")
        prob_UNC = prob_to_share_link_with_rating(curr_user_data, "UNC")

        _max = max(prob_T, prob_N, prob_UNC)
        if _max==prob_T:
            return "T"
        elif _max==prob_N:
            return "N"
        elif _max==prob_UNC:
            return "UNC"
        else:
            raise Exception("case not handled")

    res:dict = {}
    from tqdm import tqdm

    #TODO classifico solo user che condividono almeno un URL
    if classify_only_user_w_URL:
        users_info = users_info.loc[(users_info["link_url_to_expand"].notnull())]

    unique_users_id = users_info["user_id"].unique().tolist()
    for curr_user_id in tqdm(unique_users_id, desc="Calculating users features (who users that share at least one URL)...", total=len(unique_users_id)):

        curr_user_data = users_info.loc[users_info["user_id"] == curr_user_id]

        curr_user_metadata = curr_user_data.iloc[0]
        tmp_dict = {attr_name: curr_user_metadata[attr_name] for attr_name in attribute_names}

        tmp_dict["analysis1"] = process_user_for_analysis1(curr_user_data)
        tmp_dict["nec_types"] = get_user_nec_community_types(curr_user_data)
        tmp_dict["belongs_to_usrNEC"] = str( belongs_to_usrNEC(curr_user_data) )
        tmp_dict["belongs_to_urlNEC"] = str( belongs_to_urlNEC(curr_user_data) )

        numb_url = numb_url_that_shares(curr_user_data)
        tmp_dict["numb_url"] = str( numb_url )

        tmp_dict["info_diet"] = "-1"
        tmp_dict["purity_N"] = "-1"
        tmp_dict["purity_T"] = "-1"
        tmp_dict["purity_UNC"] = "-1"

        if numb_url > 0:
            tmp_dict["info_diet"] = str(estimate_info_diet(curr_user_data))
            tmp_dict["purity_N"] = str(prob_to_share_url_with_rating(curr_user_data, 0, "N"))
            tmp_dict["purity_T"] = str(prob_to_share_url_with_rating(curr_user_data, 0, "T"))
            tmp_dict["purity_UNC"] = str(prob_to_share_url_with_rating(curr_user_data, 0, "UNC"))

        #tmp_dict["prob_range_N"] = prob_range_to_share_url_with_rating(curr_user_data, "N")
        #tmp_dict["prob_range_T"] = prob_range_to_share_url_with_rating(curr_user_data, "T")
        #tmp_dict["prob_range_UNC"] = prob_range_to_share_url_with_rating(curr_user_data, "UNC")


        res[curr_user_id] = tmp_dict

    attribute_names.append("analysis1")
    attribute_names.append("nec_types")
    attribute_names.append("belongs_to_usrNEC")
    attribute_names.append("belongs_to_urlNEC")
    attribute_names.append("numb_url")
    attribute_names.append("info_diet")
    attribute_names.append("purity_N")
    attribute_names.append("purity_T")
    attribute_names.append("purity_UNC")

    return res, attribute_names