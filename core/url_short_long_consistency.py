import pandas as pd
import re
import os

import urlexpander
from urllib.parse import urlsplit, urlunsplit

def remove_dir_if_exist(target:str=None):
    if os.path.exists(target):
        import shutil
        shutil.rmtree(target)

def create_dir_if_not_exist(target:str=None):
    if not os.path.exists(target):
        try:
            os.mkdir(target)
        except OSError:
            print("Creation of the directory %s failed" % target)
        else:
            print("Successfully created the directory %s " % target)

def dfprint(df=None, tag=""):
    # print(df.info())
    # print(df.head(10))
    print("---")
    print(tag, " ", str(len(df.index)))


def map_to_dict(df=None, col1=None, col2=None):
    d: dict = {}
    for index, row in df.iterrows():
        key = row[col1]
        value = row[col2]
        if key not in d:
            d[key] = []
        d[key].append(value)
    return d


def format_url(url:str=None):
    if isinstance(url, str):
        url = url.lower()
        url = url.replace("http", "https")
        url = url.replace("httpss", "https")
        url = url.replace("https://www.", "https://")
        url = re.sub('\:[0-9]+', '', url) # handle port number
        url = urlunsplit(urlsplit(url)._replace(query="", fragment=""))
        url = url if url[-1]=="/" else url+"/" # add "/" as a final character if not present
    return url


def load_df(csv_path, delete_wo_url:bool=True) -> pd.DataFrame:
    df: pd.DataFrame = pd.read_csv(csv_path, dtype=str)

    if delete_wo_url:
        df = df[df.link_url_to_expand.notnull()]

    df['link_url_to_expand'] = df.apply(lambda row: format_url(row['link_url_to_expand']), axis=1)
    df['expanded_url'] = df.apply(lambda row: format_url(row['expanded_url']), axis=1)

    return df


def check_url_short_long_relation(csv_path):

    df = load_df(csv_path)
    df = df[['id', 'link_url_to_expand', 'expanded_url']]
    df = df.drop_duplicates(
        subset = ['link_url_to_expand', 'expanded_url'],
        keep = 'first').reset_index(drop = True)
    print(df.info())

    _1_short_to_N_long = df.groupby(pd.Grouper(key='link_url_to_expand')).size().reset_index(name="size")[
        ['link_url_to_expand', 'size']].sort_values(by="size")
    _1_short_to_N_long = _1_short_to_N_long.loc[(_1_short_to_N_long['size'] > 1)]

    _1_short_to_N_long_details = df[
        df['link_url_to_expand'].isin(list(_1_short_to_N_long['link_url_to_expand']))
    ].sort_values(by='expanded_url')


    _1_long_to_N_short: pd.DataFrame = df.groupby(pd.Grouper(key='expanded_url')).size().reset_index(name="size")[
        ['expanded_url', 'size']].sort_values(by="size")
    _1_long_to_N_short = _1_long_to_N_short.loc[(_1_long_to_N_short['size'] > 1)]


    _1_long_to_N_short_details = df[
        df['expanded_url'].isin(list(_1_long_to_N_short['expanded_url']))
    ].sort_values(by='expanded_url')

    return _1_long_to_N_short, _1_long_to_N_short_details, _1_short_to_N_long, _1_short_to_N_long_details


def check_url_short_long_relation_and_save_results(csv_path, base_path_out):

    _1_long_to_N_short, _1_long_to_N_short_details, _1_short_to_N_long, _1_short_to_N_long_details = check_url_short_long_relation(csv_path)

    _1_short_to_N_long.to_csv( os.path.join(base_path_out, "WARNING_1_short_to_N_long.csv"), index=False )
    _1_long_to_N_short.to_csv( os.path.join(base_path_out, "WARNING_1_long_to_N_short.csv"), index=False)
    _1_short_to_N_long_details.to_csv( os.path.join(base_path_out, "WARNING_1_short_to_N_long_details.csv"), index=False)
    _1_long_to_N_short_details.to_csv( os.path.join(base_path_out, "WARNING_1_long_to_N_short_details.csv"), index=False)

    # Save details

    _1_long_to_N_short_details_errors = _1_long_to_N_short_details[
        _1_long_to_N_short_details.expanded_url.str.contains('_error_', regex=True, na=False)].sort_values(
        by='expanded_url')
    _1_long_to_N_short_details_others = _1_long_to_N_short_details[
        ~_1_long_to_N_short_details.expanded_url.str.contains('_error_', regex=True, na=False)].sort_values(
        by='expanded_url')

    _1_long_to_N_short_details_errors.to_csv( os.path.join(base_path_out, "WARNING_1_long_to_N_short_details_errors.csv"), index=False)
    _1_long_to_N_short_details_others.to_csv( os.path.join(base_path_out, "WARNING_1_long_to_N_short_details_others.csv"), index=False)

    df = load_df(csv_path)
    df = df[
        df['expanded_url'].isin(list(_1_long_to_N_short['expanded_url']))
        | df['link_url_to_expand'].isin(list(_1_short_to_N_long['link_url_to_expand']))
        ].sort_values(by='expanded_url')
    df.to_csv( os.path.join(base_path_out, "total_tweets_affected_by_WARNING.csv"), index=False)

    return _1_long_to_N_short, _1_long_to_N_short_details, _1_short_to_N_long, _1_short_to_N_long_details


def solve_url_inconsistency(path_csv_in:str=None, base_path_out:str=None, prefix_final_out:str="v2_final_"):
    base_path_out_pre = os.path.join(base_path_out, "out_pre")
    base_path_out_post_step1 = os.path.join(base_path_out, "out_step1")
    base_path_out_post_step2 = os.path.join(base_path_out, "out_step2")
    base_path_out_post_step3 = os.path.join(base_path_out, "out_step3")

    path_post_vaccino_total_step1 = os.path.join(base_path_out_post_step1, os.path.basename(path_csv_in))
    path_post_vaccino_total_step2 = os.path.join(base_path_out_post_step2, os.path.basename(path_csv_in))
    path_post_vaccino_total_step3 = os.path.join(base_path_out_post_step3, os.path.basename(path_csv_in))

    path_vaccino_final_total = os.path.join(base_path_out, prefix_final_out+os.path.basename(path_csv_in))

    remove_dir_if_exist(base_path_out_pre)
    remove_dir_if_exist(base_path_out_post_step1)
    remove_dir_if_exist(base_path_out_post_step2)
    remove_dir_if_exist(base_path_out_post_step3)

    create_dir_if_not_exist(base_path_out_pre)
    create_dir_if_not_exist(base_path_out_post_step1)
    create_dir_if_not_exist(base_path_out_post_step2)
    create_dir_if_not_exist(base_path_out_post_step3)

    _1_long_to_N_short, _1_long_to_N_short_details, _1_short_to_N_long, _1_short_to_N_long_details = \
        check_url_short_long_relation_and_save_results(path_csv_in, base_path_out_pre)

    # (A) [1 short - N Long] trovare (short-Long) senza errore - sovrascrivere a Long con errore il Long senza errore
    short_long_wout_error = {}
    for index, row in _1_short_to_N_long_details[
        ~_1_short_to_N_long_details.expanded_url.str.contains('_error_', regex=True, na=False)].iterrows():
        short_long_wout_error[str(row["link_url_to_expand"])] = str(row["expanded_url"])

    # MODIFY THE DATASET
    df = load_df(path_csv_in)
    for index, row in df.iterrows():
        key = str(row["link_url_to_expand"])
        if key in short_long_wout_error:
            df.loc[index, 'expanded_url'] = short_long_wout_error[key]

    df.to_csv(path_post_vaccino_total_step1, index=False)

    _1_long_to_N_short, _1_long_to_N_short_details, _1_short_to_N_long, _1_short_to_N_long_details = \
        check_url_short_long_relation_and_save_results(path_post_vaccino_total_step1, base_path_out_post_step1)

    # (B) [1 Long - N short] trovare (short-Long) che coincidono - sovrascrivere a short la versione Long
    short_long_equals = {}
    for index, row in _1_long_to_N_short_details[
        _1_long_to_N_short_details[['link_url_to_expand', 'expanded_url']].nunique(axis=1) == 1].iterrows():
        short_long_equals[str(row["link_url_to_expand"])] = str(row["expanded_url"])

    # MODIFY THE DATASET
    df = load_df(path_post_vaccino_total_step1)
    for index, row in df.iterrows():
        key = str(row["expanded_url"])
        if key in short_long_equals:
            df.loc[index, 'link_url_to_expand'] = short_long_equals[key]

    df.to_csv(path_post_vaccino_total_step2, index=False)
    _1_long_to_N_short, _1_long_to_N_short_details, _1_short_to_N_long, _1_short_to_N_long_details = \
        check_url_short_long_relation_and_save_results(path_post_vaccino_total_step2, base_path_out_post_step2)

    # (C) [1 Long - N short] trovare (Long-[short]) e sovrascrivere su gli N short la stessa versione di Long
    long_short_list = {}
    for index, row in _1_long_to_N_short_details[
        ~_1_long_to_N_short_details.expanded_url.str.contains('_error_', regex=True, na=False)].iterrows():
        long_url = str(row["expanded_url"])
        if long_url not in long_short_list:
            long_short_list[long_url] = []
        long_short_list[long_url].append(str(row["link_url_to_expand"]))
    # keep only long with more than one short linked
    long_short_list = {key: value for key, value in long_short_list.items() if len(value) > 1}

    # MODIFY THE DATASET
    df = load_df(path_post_vaccino_total_step2)
    for index, row in df.iterrows():
        key = str(row["expanded_url"])
        if key in long_short_list:
            df.loc[index, 'link_url_to_expand'] = key

    df.to_csv(path_post_vaccino_total_step3, index=False)
    check_url_short_long_relation_and_save_results(path_post_vaccino_total_step3, base_path_out_post_step3)

    df_final_only_url = load_df(path_post_vaccino_total_step3)
    # save final dataset version (URL adjusted)
    df_final_only_url.to_csv(path_vaccino_final_total, index=False)

    df_init = load_df(path_csv_in, delete_wo_url=False)
    # delete all rows from init that are contained in step3
    df_init = df_init[df_init.link_url_to_expand.isnull()]
    # insert all rows to init that are contained in step3
    df_init_adjusted = pd.concat( [df_init, df_final_only_url] )

    df_init_adjusted.to_csv(path_vaccino_final_total, index=False)
    """main_short_domain_list: list = urlexpander.constants.all_short_domains.copy()
    _1_long_to_N_short = df[~df['resolved_domain'].isin(main_short_domain_list)]"""




