# importing the modules
import requests
from bs4 import BeautifulSoup
from typing import List
from tqdm import tqdm



def sequential_scrape_titles_from_urls(urls:list=None) -> List:
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}
    res = [] #list of dict
    errors = []

    for idx, url in enumerate( tqdm( urls, desc="Scraping titles from URLs" ) ) :
        try:
            reqs = requests.get(url, headers=headers, timeout=5)

            if reqs.status_code != 200:
                raise Exception("response code "+str(reqs.status_code))

            soup = BeautifulSoup(reqs.text, 'html.parser')
            tt = soup.find_all('title', limit=1)
            titles = [str(t.get_text()) for t in tt]
            if len( titles ) != 1 :
                raise Exception(titles + " : More than one title was found")

            title = titles[0]
            # text adjustments
            title = title.replace("\n", "")
            res.append( {"short_url" : url, "title" : title, "err" : ""} )
        except Exception as e:
            err = {"short_url" : url, "title" : "", "err" : str(e)}
            errors.append( err )
    return res, errors



