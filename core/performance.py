import pandas as pd
import core.utils as u
import datetime
import numpy as np
from typing import List
import igraph as ig
from sklearn import metrics
from pandas import to_datetime
import core.business_logic as bl


class WindowDetails(object):

    def __init__(self, unique_user_id: set=None, user_id_by_comm: dict=None):
        self._unique_user_id: set = u.require_not_None(unique_user_id)
        self._user_id_by_comm: dict = u.require_not_None(user_id_by_comm)

    def get_users(self) -> set:
        return self._unique_user_id.copy()

    def get_communities(self) -> List[int]:
        return u.require_not_None( list( self._user_id_by_comm.keys() ) )

    def get_user_ids_in_community(self, comm_id:int=None) -> set:
        u.require_not_None(comm_id)
        find:set = self._user_id_by_comm[comm_id]
        return find.copy()

def _select_data_in_interval(df: pd.DataFrame = None, from_date: datetime.date = None, to_date: datetime.date = None) -> pd.DataFrame:
    u.require_not_None(df)
    _from_ = np.datetime64(u.require_not_None(from_date).strftime('%Y-%m-%d'))
    _to_ = np.datetime64(u.require_not_None(to_date).strftime('%Y-%m-%d'))
    return df.loc[(df['from_date'] == _from_) & (df['to_date'] == _to_)]

def _select_data_in_interval_isin(df: pd.DataFrame = None, from_date: datetime.date = None, to_date: datetime.date = None, isin_values = None) -> pd.DataFrame:
    u.require_not_None(df)
    _from_ = np.datetime64(u.require_not_None(from_date).strftime('%Y-%m-%d'))
    _to_ = np.datetime64(u.require_not_None(to_date).strftime('%Y-%m-%d'))
    u.require_not_None(isin_values)
    filtered: pd.DataFrame = df.loc[(df['from_date'] == _from_) & (df['to_date'] == _to_) & (df['name'].isin(isin_values))]
    return filtered

def extract_window_level_details(df: pd.DataFrame = None, from_date: datetime.date = None, to_date: datetime.date = None) -> WindowDetails:
    u.require_not_None(df)
    u.require_not_None(from_date)
    u.require_not_None(to_date)

    filtered = _select_data_in_interval(df, from_date, to_date)
    unique_user_id: set = set( filtered['name'].unique() )

    memberships = filtered['comm'].apply(lambda x : int(x)).to_list()
    user_id_by_comm: dict = {}
    for index, row in filtered.iterrows():
        user_id: str = row['name']
        comm_id: int = int(row['comm'])
        if comm_id not in user_id_by_comm:
            user_id_by_comm[comm_id] = set()
        user_id_by_comm[comm_id].add(user_id)

    return WindowDetails(unique_user_id, user_id_by_comm)


class IntervalsDetails(object):

    def __init__(self):
        self._interval_detail_map = {}

    def _to_key(self, from_date: datetime.date = None, to_date: datetime.date = None):
        u.require_not_None(from_date)
        u.require_not_None(to_date)

        key = str(from_date).replace("-", "")+"_"+str(to_date).replace("-", "")
        return key

    def add(self, from_date: datetime.date = None, to_date: datetime.date = None, details=None):
        key = self._to_key(from_date, to_date)
        u.require_not_None(details)

        if key in self._interval_detail_map:
            raise Exception(key," : key already present")
        self._interval_detail_map[key] = details

    def get_details(self, from_date: datetime.date = None, to_date: datetime.date = None):
        key = self._to_key(from_date, to_date)

        if key not in self._interval_detail_map:
            raise Exception(key," : key not present -> invalid interval")
        return self._interval_detail_map[key]


class CommunityIDManager(object):
    def __init__(self, clustering1:List[int]=None, clustering2:List[int]=None):
        u.require_not_None(clustering1)
        u.require_not_None(clustering2)

        self._max_len = u.require_not_None(max( len(clustering1), len(clustering2)))
        self._key_occupied: dict = {}

        for comm in clustering1:
            if comm < self._max_len: self._key_occupied[comm] = comm  # occupate key
        for comm in clustering2:
            if comm < self._max_len: self._key_occupied[comm] = comm  # occupate key

        [self._get_eventually_generate_key(comm) for comm in clustering1]
        [self._get_eventually_generate_key(comm) for comm in clustering2]

    def _generate_new_key(self, _key_occupied:dict=None, _max_len:int=None) -> int:
        for candidate_key in range(_max_len):
            if candidate_key in _key_occupied:
                continue
            return candidate_key
        raise Exception("Impossible to generate key")

    def _get_eventually_generate_key(self, key:int=None):
        u.require_not_None( key )

        if key in self._key_occupied:
            return self._key_occupied[key]

        if key < self._max_len:
            self._key_occupied[key] = key #occupate key
        else:
            self._key_occupied[key] = self._generate_new_key(self._key_occupied, self._max_len) #update key

        return self._get_eventually_generate_key(key)

    def get_comm_key(self, comm:int=None):
        return self._key_occupied[ u.require_not_None( comm ) ]


def find_community_with_max_intersection(users: set=None, clustering: WindowDetails=None, already_considered:set=None) -> int:
    u.require_not_None(users)
    u.require_not_None(clustering)
    u.require_not_None(already_considered)

    max_intersection_size:int = -1
    max_idx:int = -1

    for comm2_idx in clustering.get_communities():
        intersection_size:int = len( clustering.get_user_ids_in_community(comm2_idx).intersection( users ) )
        if intersection_size > max_intersection_size and comm2_idx not in already_considered:
            max_intersection_size = intersection_size
            max_idx = comm2_idx

    return max_idx


def compute_supervised_learning_metrics(wind_det1:WindowDetails=None, wind_det2:WindowDetails=None):
    u.require_not_None(wind_det1)
    u.require_not_None(wind_det2)

    comm_idx_clustering1: set = wind_det1.get_communities()

    bal_accuracy = 0
    bal_precision = 0
    bal_recall = 0
    ignore: set = set()
    c_star: set = wind_det1.get_users().intersection(wind_det2.get_users())

    for comm1_idx in comm_idx_clustering1:

        # TODO diffents # of communities in clustering 1 and 2!
        # TODO community that maximize the intersection maybe shared between more communities: IGNORE DUPLICATES??? Not for now


        comm2_idx = find_community_with_max_intersection(wind_det1.get_user_ids_in_community(comm1_idx), wind_det2,
                                                         ignore)
        if comm2_idx == -1: continue
        if comm2_idx in ignore: raise Exception(str(comm2_idx) + " : comm idx already considered")
        # ignore.add( comm2_idx ) # TODO in order to ignore duplicate we have to maximize the metric on the others

        A: set = wind_det1.get_user_ids_in_community(comm1_idx)
        A_prime: set = wind_det2.get_user_ids_in_community(comm2_idx)

        P: set = A.intersection(c_star)
        N: set = c_star.difference(A)

        TP: set = P.intersection(A_prime)
        TN: set = N.difference(A_prime)
        FP: set = N.intersection(A_prime)
        FN: set = P.difference(A_prime)

        if ((len(TP) + len(TN) + len(FP) + len(FN)) != len(P) + len(N)): raise Exception("invalid value")
        if (len(TN) + len(FP)) != len(N): raise Exception("invalid value")
        if (len(TP) + len(FN)) != len(P): raise Exception("invalid value")

        weight = len(A) / len(wind_det1.get_users())
        #weight = len(P) / len(c_star.get_users())#TODO new definition

        curr_accuracy: int = (len(TP) + len(TN)) / (len(P) + len(N))
        curr_bal_accuracy: float = (weight) * curr_accuracy

        curr_precision: int = 0
        if (len(TP) + len(FP)) > 0:
            curr_precision = len(TP) / (len(TP) + len(FP))
        curr_bal_precision: float = (weight) * curr_precision

        curr_recall: int = 0
        if (len(TP) + len(FN)) > 0:
            curr_recall = len(TP) / (len(TP) + len(FN))
        curr_bal_recall: float = (weight) * curr_recall

        bal_accuracy = bal_accuracy + curr_bal_accuracy
        bal_precision = bal_precision + curr_bal_precision
        bal_recall = bal_recall + curr_bal_recall

    return bal_accuracy, bal_precision, bal_recall


class MetricsCalculator(object):

    def __init__(self, intervals: list = None):
        u.require_not_None(intervals)

        self.computed = None #stub object
        self.intervals = intervals
        self.size = len( intervals )

        self.union = np.zeros((self.size, self.size))
        self.inter = np.zeros((self.size, self.size))
        self.jaccard = np.zeros((self.size, self.size))
        self.diff = np.zeros((self.size, self.size))
        self.vi = np.zeros((self.size, self.size))
        self.split_join_distance = np.zeros((self.size, self.size))#split-join
        self.rand_score = np.zeros((self.size, self.size))
        self.adjusted_rand_score = np.zeros((self.size, self.size))
        self.adjusted_mutual_info_score = np.zeros((self.size, self.size))
        self.normalized_mutual_info_score = np.zeros((self.size, self.size))
        self.mutual_info_score = np.zeros((self.size, self.size))
        self.homogeneity_score = np.zeros((self.size, self.size))
        self.completeness_score = np.zeros((self.size, self.size))
        self.v_measure_score = np.zeros((self.size, self.size))
        self.fowlkes_mallows_score = np.zeros((self.size, self.size))
        self.silhouette_score = np.zeros((self.size, self.size))
        self.calinski_harabasz_score = np.zeros((self.size, self.size))

        self.total_bal_accuracy = np.zeros((self.size, self.size))
        self.total_bal_precision = np.zeros((self.size, self.size))
        self.total_bal_recall = np.zeros((self.size, self.size))

    @staticmethod
    def compute_sliding_window(csv_in_path: str=None,
                               from_date: datetime.date = None,
                               to_date: datetime.date = None,
                               window_shift_days: int = 1,
                               window_size_days: int = 7):

        user_swind: pd.DataFrame = pd.read_csv(csv_in_path, dtype=str)
        user_swind['from_date'] = to_datetime(user_swind['from_date'], format='%Y-%m-%d').dt.tz_localize(None)
        user_swind['to_date'] = to_datetime(user_swind['to_date'], format='%Y-%m-%d').dt.tz_localize(None)

        intervals = u.generate_sliding_window_isoformat_intervals(from_date, to_date, window_shift_days, window_size_days)
        m: MetricsCalculator = MetricsCalculator( intervals )
        m._compute(user_swind)
        return m


    def _compute(self, user_swind: pd.DataFrame=None):
        u.require_not_None(user_swind)
        if self.computed is not None: raise Exception("metrics alredy computed!")

        for wind1_idx in range(self.size):
            for wind2_idx in range(self.size):
                interval1 = self.intervals[wind1_idx]
                interval2 = self.intervals[wind2_idx]

                wind_det1: WindowDetails = extract_window_level_details(user_swind, interval1[0], interval1[1])
                wind_det2: WindowDetails = extract_window_level_details(user_swind, interval2[0], interval2[1])

                # WINDOW LEVEL STATISTICs
                self.union[wind1_idx][wind2_idx] = len(wind_det1.get_users().union(wind_det2.get_users()))
                self.inter[wind1_idx][wind2_idx] = len(wind_det1.get_users().intersection(wind_det2.get_users()))
                self.diff[wind1_idx][wind2_idx] = len(wind_det2.get_users().difference(wind_det1.get_users()))
                self.jaccard[wind1_idx][wind2_idx] = (self.inter[wind1_idx][wind2_idx] / self.union[wind1_idx][wind2_idx])

                # Clustering comparison metrics
                bal_accuracy, bal_precision, bal_recall = compute_supervised_learning_metrics(wind_det1, wind_det2)
                self.total_bal_accuracy[wind1_idx][wind2_idx] = bal_accuracy
                self.total_bal_precision[wind1_idx][wind2_idx] = bal_precision
                self.total_bal_recall[wind1_idx][wind2_idx] = bal_recall

                user_id_inter = wind_det1.get_users().intersection(wind_det2.get_users())
                clustering1_on_inter: list = \
                    _select_data_in_interval_isin(user_swind, interval1[0], interval1[1], isin_values=user_id_inter)[['name', 'comm']]\
                        .sort_values(by=['name'], ascending=True)['comm']\
                        .apply(lambda x: int(x)).tolist()

                clustering2_on_inter: list = \
                    _select_data_in_interval_isin(user_swind, interval2[0], interval2[1], isin_values=user_id_inter)[['name', 'comm']]\
                        .sort_values(by=['name'], ascending=True)['comm']\
                        .apply(lambda x: int(x)).tolist()

                mng: CommunityIDManager = CommunityIDManager(clustering1_on_inter,
                                                             clustering2_on_inter)  # sometimes the comm id it's greater than the number of selected edge in the projection
                clustering1_comm = [mng.get_comm_key(key) for key in clustering1_on_inter]
                clustering2_comm = [mng.get_comm_key(key) for key in clustering2_on_inter]

                self.vi[wind1_idx][wind2_idx] = ig.clustering.compare_communities(comm1=clustering1_comm, comm2=clustering2_comm, method='meila')

                self.split_join_distance[wind1_idx][wind2_idx] = ig.clustering.compare_communities(comm1=clustering1_comm, comm2=clustering2_comm, method='split-join')

                self.rand_score[wind1_idx][wind2_idx] = metrics.rand_score(clustering1_comm, clustering2_comm)
                self.adjusted_rand_score[wind1_idx][wind2_idx] = metrics.adjusted_rand_score(clustering1_comm, clustering2_comm)
                self.adjusted_mutual_info_score[wind1_idx][wind2_idx] = metrics.adjusted_mutual_info_score(clustering1_comm, clustering2_comm)
                self.normalized_mutual_info_score[wind1_idx][wind2_idx] = metrics.normalized_mutual_info_score(clustering1_comm, clustering2_comm)
                self.mutual_info_score[wind1_idx][wind2_idx] = metrics.mutual_info_score(clustering1_comm, clustering2_comm)
                self.homogeneity_score[wind1_idx][wind2_idx] = metrics.homogeneity_score(clustering1_comm, clustering2_comm)
                self.completeness_score[wind1_idx][wind2_idx] = metrics.completeness_score(clustering1_comm, clustering2_comm)
                self.v_measure_score[wind1_idx][wind2_idx] = metrics.v_measure_score(clustering1_comm, clustering2_comm)
                self.fowlkes_mallows_score[wind1_idx][wind2_idx] = metrics.fowlkes_mallows_score(clustering1_comm, clustering2_comm)
                # silhouette_score[wind1_idx][wind2_idx] = metrics.silhouette_score(clustering1_comm, clustering2_comm)
                # calinski_harabasz_score[wind1_idx][wind2_idx] = metrics.calinski_harabasz_score(clustering1_comm, clustering2_comm)

        self.computed = {} #stub init

    def append_only_contiguos_windows_metrics(self, exporter: u.FileExporterManager=None, s:bl.SWExperimentalSettings=None):
        u.require_not_None(exporter)
        if self.computed is None: raise Exception("metrics not computed yet!")

        def interval_to_string(d1: datetime.date = None, d2: datetime.date = None):
            u.require_not_None(d1)
            u.require_not_None(d2)
            return str(d1.month) + "-" + str(d1.day) + " / " + str(d2.month) + "-" + str(d2.day)

        future_write = []
        for idx_t0 in range(self.size - 1):
            idx_t1 = idx_t0 + 1
            interval_t0 = self.intervals[idx_t0]
            interval_t1 = self.intervals[idx_t1]

            intervals:str = "[" + interval_to_string(interval_t0[0], interval_t0[1]) + "] - [" + interval_to_string(
                    interval_t1[0], interval_t1[1]) + "]"

            future_write.append({
                'intervals': intervals,
                'w_size': s.window_size_days,
                'w_shift': s.window_shift_days,
                'union': self.union[idx_t0][idx_t1],
                'inter': self.inter[idx_t0][idx_t1],
                'diff': self.diff[idx_t0][idx_t1] + self.diff[idx_t1][idx_t0],
                'jaccard': self.jaccard[idx_t0][idx_t1],
                'vi': self.vi[idx_t0][idx_t1],
                'split_join_distance': self.split_join_distance[idx_t0][idx_t1],
                'bal_accuracy': self.total_bal_accuracy[idx_t0][idx_t1],
                'bal_precision': self.total_bal_precision[idx_t0][idx_t1],
                'bal_recall': self.total_bal_recall[idx_t0][idx_t1],
                'rand_score': self.rand_score[idx_t0][idx_t1],
                'adjusted_rand_score': self.adjusted_rand_score[idx_t0][idx_t1],
                'adjusted_mutual_info_score': self.adjusted_mutual_info_score[idx_t0][idx_t1],
                'normalized_mutual_info_score': self.normalized_mutual_info_score[idx_t0][idx_t1],
                'mutual_info_score': self.mutual_info_score[idx_t0][idx_t1],
                'homogeneity_score': self.homogeneity_score[idx_t0][idx_t1],
                'completeness_score': self.completeness_score[idx_t0][idx_t1],
                'v_measure_score': self.v_measure_score[idx_t0][idx_t1],
                'fowlkes_mallows_score': self.fowlkes_mallows_score[idx_t0][idx_t1]
            })

        exporter.append( future_write )








